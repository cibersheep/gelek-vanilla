v3.1.0
-Updated remglk to v0.3.1
-Updated terps: Hugo v3.1.06, Level9 v5.2, Magnetic v2.3.1, Scare v1.3.10, TADS2 and TADS3
-Fix small bugs
-Changed version of Adventure available

v3.0.0
-Update to Focal

v2.1.4
- Fix for games with timer stopping on input
- Fix on games with animation stopping / waiting for it to finish

v2.1.3
- TADS games work now on arm64 (Thanks cspiegel)
- Keyboard will not jump. Advice: DISABLE typing aids from System Settings
- Updated Babel for better guess games
- Updated TADS interpreter

v2.1.2
- Updated Franch tanslation (thanks Anne Onyme)

v2.1.1
- Fix size and ratio of images (hopefully :D)

v2.1.0
- Added theme chooser
- Onscreen special keys (cursors and tabs). Improved character management
- Support for phisycal keyboard cursor and tab keys
- Fixed automatic scrolling text and better focus management
- Bigger button areas
- Better code for ContentHub
- Tweaked unzip to skip existent files
- Updated databse website source
- App as binary (Thanks Jonnius)
- Debugging will also show c++ messages
- Updated Catalan translation
- Code cleaning (app should be a bit faster with big images)

v2.0.0
- Added search in the imported games list
- Added game download page
- Enable unzip zip files
- Terp chooser for game (If a game is not correctly detected, you can choose how to run it)
- Import chooser (import more than 1 file and you'll be asked for which to run)
- Moved c++ database to sqlite
- Saved Games ordered by game name or IFID
- Fixes GameInfo Dialog
- Fixed deleting non existent files crash
- Add untranslated strings and update pot
- Updated Catalan translation
- Code cleaning

v1.9.0
- Updated remGLK
- Updated several interpreters
- Code and layout cleaning
- Fixed headers and overlapping images on Level 9 games
- Better game detection
- Fixed ifiction files not updatable
- Updated About page

v1.8.1
- Updated French translation (thanks Anne Onyme 017)
- Quick fix for saving/restoring TADS games

v1.8.0
- fixed not able to save under certain circumstances (terp giving error when cancelling)
- better error handling
- fixed terp stopping when pressed Esc to dismiss save/restore Dialog
- fixed timer being overlooked (fixes playing «Ke rulen los petas»)
- fixed inline images being ignored
- unified all only-text terps under one qml page
- added missing scott terp
- added desktop keywords
- updated Catalan translation
- fixed SuruDark errors
- cleaned codes
- deleted unused qml files
- updated About page

v1.7.1
- Fix saved games list not showing savedgames

v1.7.0
- Updated project structure
- Fixed issues with interpreter chooser
- Added advsys and adrift interpreters
- Fixed image size
- Fixed images not showing in glulx
- Build for arm64

v1.6.7
- Update App Name in desktop File

v1.6.6
 - Fixes games not working on fresh app install (thanks Katherine)
- Fixes first game imported not showing in the list
- Fixed About typo
- Updated translations
- Qml clean up and small changes to code

v1.6.5
 - Added text styles
 - Fixed preformatted text line break
 - Empty images don't show empty space
 - Added ScottAddams and Agility interpreters
 - No automatic scroll
 - No debug info text

v1.5.5
 - Fixed saving issue when using TADS games

v1.5.1
 - Fixed saving issue on L9 games
 - Fixed missed saved game icon
 - Fixed zblorb not recognized correctly
 - Other minor bugs

v1.5.0:
 - Added support for TADS, Magnetic Scroll and Glulxe interprets
 - Updated Catalan translation
 - Fixed bug stopping games if action was entered while drawing an image

v1.2.1
 - Changed interpreter fortz with bocfel
 - Added changes for Level 9 games from Gelek
 - better image size
 - mini menu for small screens
 - little bug fixes
 - Initial support for z6 games and grid text
 - trunkated json bug fixed
 - Support for Utf8 characters (non English games)

v1.1
 - Bug fixes

v1.0
 - Initial commit

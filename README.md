[![OpenStore](https://img.shields.io/badge/Install%20from-OpenStore-000000.svg)](https://open-store.io/app/gelek-vanilla.cibersheep)

# Gelek Vanilla
Interactive fiction player using Qt and remglk implementation for Ubuntu Touch OS

## About
App to be able to play interactive fiction games. Import games, manage save files, read company info, instructions for all games, access to most common action on bottom edge menu (or right side if in convergence mode).

Why vanilla? Because it has no information about any game.

Features:
- Supported game formats: Adrift, Agt, Glulx, Level 9 Computing, Magnegitc Scrolls, Scott Adams, TADS, Z-machine (more to come)
- The app downloads information of games from ifdb.org
- Auto-detects game format

Known issues:
- Some games doesn't work properly (if they having more than one window of each kind)
- Some pictures behave weirdly
- Some games ask to tap up/down keys: that can be done pressing space
- Some games ask to enter a letter for some menu entries and game stops responding (needs a space behind the letter or disable spellchecking)
- In game menu options of Beyond Zork can be accessed only buy the first letter

## Changelog
See [CHANGELOG.md]

## Building
1st Run `./build-Gelek.sh` to compile libs for armhf, arm64 and amd64 architectures
2nd `clickable desktop` to run it on desktop or `clickable` to build the app for armhf architecture, `clickable -a amd64` to build the app for arm64

## Links
Gelek Development: Joan CiberSheep
https://twitter.com/cibersheep

Code Used from Andrew Plotkin remglk (MIT) https://github.com/erkyrath/remglk and 
glk (MIT) http://eblong.com/zarf/glk/ 

Code Used from Chris Spiegel, David Betz, Robert Masenten, Thomas Ljungsbro, Kent Tessman, Niclas Karlsson, Tor Andersson, Ben Cressey, Simon Baldwin, Glen Summers and contributors
https://github.com/cspiegel/terps

Code Used from Wake Reality Qt glk implementation (BSD-2-Clause) 
https://github.com/WakeRealityDev/Thunderquake_Qt 

---

Special Thanks to Jonatan «Jonny» for his help setting up libraries build tool chain https://gitlab.com/jonnius/

Special Thanks to Brian Douglass for his help with the code, the language and concept, and beta testing http://bhdouglass.com/ 

Special Thanks to Paul Jujuyeh for his additional C++ code and support http://patreon.com/jujuyeh 

Special Thanks to Mikel Larrea for his beta testing and feed back https://github.com/LarreaMikel 

App Icon CC-By Kennen Yordle https://thenounproject.com/kennen.yordle/ 

Computer Icon CC-By Matthew Hawdon https://thenounproject.com/matthawdon/ 

Save Icon Base CC-By i cons https://thenounproject.com/iconsguru/ 

## License

Copyright (C) 2019  Joan CiberSheep

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.

See libs/terps and libs/remglk for each project license

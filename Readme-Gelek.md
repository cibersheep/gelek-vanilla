# Gelek Vanilla
## Known issues

I have been finding weird behaviours. Have this in mind prior to build Gelek Vanilla:

Terps from upstream: https://github.com/cspiegel/terps
 -
 
### TADS issues

- TADS will not save unless uses `strcpy(fname_buf, garglk_fileref_get_name(fileref));` See [commit](https://gitlab.com/cibersheep/gelek-vanilla/-/commit/ec3d5542804d584ae0e499d3d4d6b2ec484c461e#9f707ef0792c6066436b80d9745cc61f1b3ae591_543_540)

- arm64 building problems (old?):
    in tads/tads2/os.h change
    `#if defined __x86_64__`
    for
    `#if defined __x86_64__ || defined __aarch64__`

    in tads/CmakeLists.txt
        - Add to the begining: set (CMAKE_CXX_STANDARD 11)
        - Add: GLK_ANSI_ONLY  to  `set(macros xxx`


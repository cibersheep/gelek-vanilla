#!/bin/bash

#In the latest release Alan2 terp doesn't build. So you can `clickable build-libs terps` and select whitch ones you need
#Build libraries
echo "Building libraries\n---------------\n"
echo "• arm\n"
clickable build --libs -a armhf
echo "• arm64\n"
clickable build --libs -a arm64
echo "• amd64\n"
clickable build --libs -a amd64

echo
echo
echo "Building app\n---------------\n"
echo "• arm\n"
clickable build -a armhf
echo "• arm64\n"
clickable build -a arm64
echo "• amd64\n"
clickable build -a amd64
echo
echo "Done\n---------------\n"

#!/bin/bash

#In the latest release Alan2 terp doesn't build. So you can `clickable build-libs terps` and select whitch ones you need
#Build libraries
echo "Building libraries\n---------------\n"
echo "\nremglk:\n"
echo "• amd64\n"
clickable build --libs remglk -a amd64

echo "\nCopying remglk lib into terps folder:"
echo "• Creating folders\n"
mkdir -p build/x86_64-linux-gnu/terps/remglk

echo "• Copying remlgk to terps/remglk folder\n"
cp build/x86_64-linux-gnu/remglk/*    build/x86_64-linux-gnu/terps/remglk

echo "\nBuilding terps:"
echo "• amd64\n"
clickable build --libs terps -a amd64

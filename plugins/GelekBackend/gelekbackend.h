/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef GELEKBACKEND_H
#define GELEKBACKEND_H

#include <QObject>
#include <QProcess>

class GelekBackend: public QObject {
    Q_OBJECT

public:
    GelekBackend();
    ~GelekBackend() = default;

    Q_INVOKABLE bool setDebuggingMode(bool debugging);
    Q_INVOKABLE void systemInfo();
    Q_INVOKABLE bool launchTerp(const QString terp, const QString game);
    Q_INVOKABLE void stopTerp();
    Q_INVOKABLE void sendCommand(const QString cmd);
    Q_INVOKABLE QString readSOL();
    Q_INVOKABLE QString storeGameFile(QString gameURL, QString gameName);
    Q_INVOKABLE void createSavedGamesDir();
    Q_INVOKABLE void deleteSavedGames(QString savedGame);

    Q_INVOKABLE QStringList listFiles();
    Q_INVOKABLE void babel(QString request, const QString game);
    Q_INVOKABLE QString readBabelOut();
    Q_INVOKABLE void extractBlorb(const QString game, const QString ifid);
    Q_INVOKABLE QString readBlorbOut();
    Q_INVOKABLE void addToList(const QString savefile, const QString game, const QString dateTime);
    Q_INVOKABLE QString whichGame(const QString savefile);
    Q_INVOKABLE void whatTime(const QString savefile, QString dateTime);
    Q_INVOKABLE void deleteSave(const QString savefile);
    Q_INVOKABLE QStringList exportSave();
    Q_INVOKABLE QString standardPathTo(int path);
    Q_INVOKABLE void firtRunFolderSetUp();

Q_SIGNALS:
    void stdoutLevel();
    void stdoutBabel(const QString game);
    void stdoutBlorb();

public slots:
    void processOutput();
    void babelOutput();
    void blorbOutput();

protected:
    QProcess *terpProcess;
    QProcess *babelProcess;
    QProcess *blorbProcess;
};
#endif


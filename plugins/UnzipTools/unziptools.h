/*
 * Copyright (C) 2019  Joan CiberSheep
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef UNZIPTOOLS_H
#define UNZIPTOOLS_H

#include <QObject>
#include <QProcess>

class UnzipTools: public QObject {
    Q_OBJECT

public:
    UnzipTools();
    ~UnzipTools() = default;

    Q_INVOKABLE void extractZip(const QString fileName, const QString path, const QString destination);
    Q_INVOKABLE void extractTar(const QString path, const QString destination);
    Q_INVOKABLE void extractGzipTar(const QString path, const QString destination);
    Q_INVOKABLE void extractBzipTar(const QString path, const QString destination);
    Q_INVOKABLE void cancelArchiveExtraction();

signals:
    void finished(bool success, int errorCode);
    void killProcess();

Q_SIGNALS:
    void fileExtracted();

private slots:
    void _onError(QProcess::ProcessError error);
    void _onFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    void extractArchive(const QString program, const QStringList arguments, const QString path);

    QProcess* _process = nullptr;

};
#endif


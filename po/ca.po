# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-09-18 19:45+0000\n"
"PO-Revision-Date: 2024-09-18 21:51+0200\n"
"Last-Translator: Joan CiberSheep <cibersheep@gmail.com>\n"
"Language-Team: \n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.2\n"

#: ../qml/About.qml.in:39
msgid "About"
msgstr "Quant a"

#: ../qml/About.qml.in:94
msgid "Version %1. Source %2"
msgstr "Versió %1. Codi font %2"

#: ../qml/About.qml.in:105
msgid "Under License %1"
msgstr "Sota llicència  %1"

#: ../qml/About.qml.in:141 ../qml/About.qml.in:146 ../qml/About.qml.in:151
#: ../qml/About.qml.in:156 ../qml/About.qml.in:161
msgid "Code Used from"
msgstr "S'ha usat codi de"

#: ../qml/About.qml.in:167 ../qml/About.qml.in:172 ../qml/About.qml.in:177
msgid "Special Thanks to"
msgstr "Agraïments especials per"

#: ../qml/About.qml.in:183 ../qml/About.qml.in:188 ../qml/About.qml.in:193
#: ../qml/About.qml.in:198 ../qml/About.qml.in:203 ../qml/About.qml.in:208
#: ../qml/About.qml.in:213 ../qml/About.qml.in:218 ../qml/About.qml.in:223
#: ../qml/About.qml.in:228
msgid "Accepted formats"
msgstr "Formats acceptats"

#: ../qml/About.qml.in:185 ../qml/About.qml.in:190 ../qml/About.qml.in:195
#: ../qml/About.qml.in:200 ../qml/About.qml.in:205 ../qml/About.qml.in:210
#: ../qml/About.qml.in:215 ../qml/About.qml.in:220 ../qml/About.qml.in:225
#: ../qml/About.qml.in:230
msgid "File suffix: %1"
msgstr "Extensió: %1"

#: ../qml/About.qml.in:234 ../qml/About.qml.in:239 ../qml/About.qml.in:244
#: ../qml/About.qml.in:249
msgid "Icons"
msgstr "Icones"

#: ../qml/DownloadsPage.qml:16
msgid "Selected Games"
msgstr "Jocs seleccionats"

#: ../qml/DownloadsPage.qml:32
msgid "Unsorted Game List"
msgstr "Llista de jocs variats"

#: ../qml/DownloadsPage.qml:46
msgid "Several available games"
msgstr "Un parell de jocs disponibles"

#: ../qml/DownloadsPage.qml:77
msgid "Open link"
msgstr "Obre l'enllaç"

#: ../qml/DownloadsPage.qml:86
msgid "Language: %1 Format: %2"
msgstr "Idioma: %1 Format: %2"

#: ../qml/ImportPage.qml:38
msgid "Choose"
msgstr "Trieu"

#: ../qml/Main.qml:158
msgid "Importing List of Saved Games"
msgstr "Important la llista dels jocs desats"

#: ../qml/MainPage.qml:19 gelek-vanilla.desktop.in.h:1
msgid "Gelek Vanilla"
msgstr "Gelek Vainilla"

#: ../qml/MainPage.qml:68
msgid "Import"
msgstr "Importa"

#: ../qml/MainPage.qml:77
msgid "a new game"
msgstr "un nou joc"

#: ../qml/MainPage.qml:81
msgid "Accepted formats: %1"
msgstr "Formats acceptats: %1"

#: ../qml/MainPage.qml:115
msgid "No Imported Games Yet"
msgstr "No s'ha importat cap joc encara"

#: ../qml/MainPage.qml:115
msgid "Previouly Imported Games"
msgstr "Jocs importats prèviament"

#: ../qml/MainPage.qml:184 ../qml/SavedGamesPage.qml:18
msgid "Saved Games"
msgstr "Jocs desats"

#: ../qml/MainPage.qml:199
msgid "Download Games"
msgstr "Descarrega els jocs"

#: ../qml/PlayGlulx.qml:48
msgid "Glulx Interpreter"
msgstr "Intèrpret Glulx"

#: ../qml/PlayLevel9.qml:41
msgid "Level 9 interpreter"
msgstr "Intèrpret Level9"

#: ../qml/PlayMagnetic.qml:47
msgid "Magnetic Scrolls interpreter"
msgstr "Intèrpret Magnetic Scrolls"

#: ../qml/PlayTads2.qml:46
msgid "TADS Interpreter"
msgstr "Intèrpret TADS"

#: ../qml/SavedGamesPage.qml:34
msgid "List of Saved Games"
msgstr "Llistat de jocs desats"

#: ../qml/SavedGamesPage.qml:49
msgid "No saved games found"
msgstr "No s'ha trobat cap joc desat"

#: ../qml/SavedGamesPage.qml:50
msgid "Slide for options"
msgstr "Llisque per opcions"

#: ../qml/SavedGamesPage.qml:81 ../qml/components/GameListView.qml:21
msgid "Delete"
msgstr "Elimina"

#: ../qml/SavedGamesPage.qml:95
msgid "Share"
msgstr "Comparteix"

#: ../qml/SavedGamesPage.qml:110 ../qml/components/SavePopup.qml:34
msgid "Save"
msgstr "Desa"

#: ../qml/SavedGamesPage.qml:167
msgid "Unknown"
msgstr "Desconegut"

#: ../qml/SettingsPage.qml:16 ../qml/components/GelekHeader.qml:31
#: ../qml/components/MainHeader.qml:68
msgid "Settings"
msgstr "Configuració"

#: ../qml/SettingsPage.qml:44
msgid "General Settings"
msgstr "Configuració general"

#: ../qml/SettingsPage.qml:51
msgid "Theme"
msgstr "Tema"

#: ../qml/SettingsPage.qml:75
msgid "Convergence Settings"
msgstr "Configuració de convergència"

#: ../qml/SettingsPage.qml:92
msgid "Convergence"
msgstr "Covergència"

#: ../qml/SettingsPage.qml:109
msgid "Pictures to the right"
msgstr "Imatges a la dreta"

#: ../qml/SettingsPage.qml:133
msgid "Show menu to the right"
msgstr "Mostra el menú a la dreta"

#: ../qml/SettingsPage.qml:153
msgid "Game Settings"
msgstr "Configuració de joc"

#: ../qml/SettingsPage.qml:169
msgid "Show ingame pictures"
msgstr "Mostra les imatges"

#: ../qml/SettingsPage.qml:186
msgid "Don't force focus on input"
msgstr "No forcis que el focus sigui a l'entrada de text"

#: ../qml/SettingsPage.qml:200
msgid "Development Settings"
msgstr "Paràmetres de desenvolupament"

#: ../qml/SettingsPage.qml:215
msgid "Enable debug logs"
msgstr "Activa el registre de depuració"

#: ../qml/SharePage.qml:36
msgid "Share with"
msgstr "Comparteix amb"

#: ../qml/SharePage.qml:37
msgid "Save with"
msgstr "Desa amb"

#: ../qml/components/BottomEdgeMenuL9.qml:11
msgid "Actions"
msgstr "Accions"

#: ../qml/components/BottomEdgeMenuL9.qml:32
msgid "Common Actions"
msgstr "Accions comunes"

#: ../qml/components/DownloadingDialog.qml:29
msgid "Download in progress"
msgstr "Descàrrega en procés"

#: ../qml/components/DownloadingDialog.qml:30
msgid "The game %1 will be added to the list of games"
msgstr "El joc %1 s'afegirà a la llista"

#: ../qml/components/DownloadingDialog.qml:52
#: ../qml/components/ExtractingDialog.qml:52
#: ../qml/components/GameInfoPopup.qml:129
#: ../qml/components/RestorePopup.qml:58 ../qml/components/SavePopup.qml:62
msgid "Cancel"
msgstr "Cancel·la"

#: ../qml/components/ErrorDialog.qml:26
msgid "It has been a problem downloading the file."
msgstr "Hi ha hagut un error al descarregar el fitxer."

#: ../qml/components/ErrorDialog.qml:28
msgid "Error"
msgstr "Error"

#: ../qml/components/ErrorDialog.qml:32 ../qml/components/GameInfoPopup.qml:130
#: ../qml/components/MainHeader.qml:47
#: ../qml/components/MultipleImportsPopup.qml:46
msgid "Close"
msgstr "Tanca"

#: ../qml/components/ExtractingDialog.qml:27
msgid "The file is being uncompressed"
msgstr "L'arxiu s'està descoporimint"

#: ../qml/components/ExtractingDialog.qml:29
msgid "Uncompressing"
msgstr "Descomprimint"

#: ../qml/components/GameInfoPopup.qml:13
msgid "Game Info"
msgstr "Informació del joc"

#: ../qml/components/GameInfoPopup.qml:78
msgid "Format: %1"
msgstr "Format: %1"

#: ../qml/components/GameInfoPopup.qml:87
msgid "Ifiction file doesn't exist!"
msgstr "El fitxer «Ifiction» no existeix"

#: ../qml/components/GameInfoPopup.qml:109
msgid "Download"
msgstr "Descarrega"

#: ../qml/components/GameInfoPopup.qml:110
msgid "Re-download"
msgstr "Torna a descarregar"

#: ../qml/components/GameInfoPopup.qml:210
msgid "Authors: %1"
msgstr "Autoria: %1"

#: ../qml/components/GameListView.qml:40
msgid "Swap interpreter"
msgstr "Canvieu d'intèrpret"

#: ../qml/components/GameListView.qml:52
msgid "Info"
msgstr "Info"

#: ../qml/components/GelekHeader.qml:15 ../qml/components/MainHeader.qml:58
msgid "Information"
msgstr "Informació"

#: ../qml/components/MainHeader.qml:38
msgid "Search name…"
msgstr "Cerca per nom…"

#: ../qml/components/MainHeader.qml:77
msgid "Search"
msgstr "Cerca"

#: ../qml/components/MultipleImportsPopup.qml:13
msgid "Files imported"
msgstr "Arxius importats"

#: ../qml/components/MultipleImportsPopup.qml:30
msgid "Choose the game you want to play"
msgstr "Trieu el joc al que voleu jugar"

#: ../qml/components/RestorePopup.qml:12
msgid "Restore Game"
msgstr "Restaura un joc"

#: ../qml/components/RestorePopup.qml:13
msgid "Choose a saved game to restore"
msgstr "Treieu un joc desat per restaurar"

#: ../qml/components/RestorePopup.qml:13
msgid "There's no saved games yet"
msgstr "Encara no hi ha cap joc desat"

#: ../qml/components/SavePopup.qml:15
msgid "Save Game"
msgstr "Desa el joc"

#: ../qml/components/SavePopup.qml:16
msgid "Enter a name to save the game"
msgstr "Escriviu un nom pel joc desat"

#: ../qml/components/SavePopup.qml:22
msgid "File name already exists!"
msgstr "Ja existeix el nom de fitxer"

#: ../qml/components/TerpSwapPopup.qml:11
msgid "Choose an Interpreter"
msgstr "Trieu un intèrpret"

#: ../qml/components/TerpSwapPopup.qml:81
msgid "Choose the default interpreter for this game"
msgstr "Trieu l'intèrpret per defecte per aquest joc"

#: ../qml/components/TerpSwapPopup.qml:104
msgid "Done"
msgstr "Fet"

#: ../qml/js/gameFormatHelper.js:6
msgid "AdvSys interpreter"
msgstr "Intèrpret AdvSys"

#: ../qml/js/gameFormatHelper.js:9
msgid "Adrift interpreter"
msgstr "Intèrpret Adrift"

#: ../qml/js/gameFormatHelper.js:12 ../qml/js/gameFormatHelper.js:55
msgid "Agility interpreter"
msgstr "Intèrpret Agility"

#: ../qml/js/gameFormatHelper.js:17
msgid "Alan3 interpreter"
msgstr "Intèrpret Alan3"

#: ../qml/js/gameFormatHelper.js:20
msgid "Alan2 interpreter"
msgstr "Intèrpret Alan2"

#: ../qml/js/gameFormatHelper.js:31
msgid "Bocfel interpreter"
msgstr "Intèrpret Bocfel"

#: ../qml/js/gameFormatHelper.js:34
msgid "Hugo interpreter"
msgstr "Intèrpret Hugo"

#: ../qml/js/gameFormatHelper.js:59
msgid "Scott Adams interpreter"
msgstr "Intèrpret Scott Adams"

#: gelek-vanilla.desktop.in.h:2
msgid ""
"interactive fiction;glk;if;text adventure;multi-system;retro;interpreter;"
"game;"
msgstr ""
"ficció interactiva;glk;if;aventura de text, conversacional;multisistema;"
"retro;intèrpret;joc;"

msgid "App Development"
msgstr "Desenvolupament de l'app"

#~ msgid "Images"
#~ msgstr "Imatges"

#~ msgid "App Icon"
#~ msgstr "Icona de l'app"

#~ msgid "Computer Icon"
#~ msgstr "Icona ordinador"

#~ msgid "Save Icon Base"
#~ msgstr "Base de la icona desar"

#~ msgid "Under Lincense %1"
#~ msgstr "Sota llicència %1"

#~ msgid "Gelek"
#~ msgstr "Gelek"

#~ msgid "Level 9 Saved Game"
#~ msgstr "Joc desat de Level 9"

#~ msgid ""
#~ "Accepted formats: zcode: .z*, level9: .atr, .bin, .com, .dat, .l9, .sna, ."
#~ "st, .tap"
#~ msgstr ""
#~ "Format acceptats: zcode: .z*, level9: .atr, .bin, .com, .dat, .l9, .sna, ."
#~ "st, .tap"

#~ msgid "Index"
#~ msgstr "Índex"

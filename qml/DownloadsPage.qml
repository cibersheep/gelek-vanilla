import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.DownloadManager 1.2

import GelekBackend 1.0
import "components"

Page {
    id: gameDownloadPage
    anchors.fill: parent

    header: GelekHeader {
        id: levelSelectedGamesHeader
        title: i18n.tr("Selected Games")
        flickable: selectedGames
    }

    property int sectionNumber

    ListView {
        id: selectedGames
        anchors.fill: parent

        header: Column {
            id: savedGameTitle
            spacing: units.gu(2)
            width: parent.width

            Label {
                text: i18n.tr("Unsorted Game List")
                color: darkColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
                width: parent.width
                height: units.gu(10)
                font.bold: true
                textSize: Label.Large
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.backgroundSecondaryText

                text: i18n.tr("Several available games")
            }
        }

        section {
            property: "category"
            criteria: ViewSection.FullString

            delegate: ListItemHeader {
                title: format
            }
        }

        model: selectedGamesModel
        delegate: selectedGamesDelegate
    }

    Component {
        id: selectedGamesDelegate

        ListItem {
            property string wichGame;

            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            trailingActions: ListItemActions {
                actions: Action {
                    iconName: "external-link"
                    text: i18n.tr("Open link")

                    onTriggered: Qt.openUrlExternally(infoUrl)
                }
            }

            ListItemLayout {
                id: showItem
                title.text: urlTitle
                subtitle.text: i18n.tr("Language: %1 Format: %2").arg(lang).arg(format)
            }

            onClicked: {
                downloadGame.downloadTitle = urlTitle
                downloadGame.download(gameUrl)

                PopupUtils.open(
                    downloadingDialog,
                    gameDownloadPage,
                    { "fileName" : urlTitle }
                )
            }
        }
    }

    ListModel {
        id: selectedGamesModel

        Component.onCompleted: selectedGamesModel.append([
            {format: "Level 9", lang: "En", urlTitle:"Snowball", gameUrl: "https://gitlab.com/cibersheep/gelek/-/raw/master/Game/Snowball.l9", infoUrl: "https://ifdb.org/viewgame?id=6lgu6t1f65qrdf7o"},
            {format: "Hugo", lang: "En", urlTitle:"Escape from Ice Station Hippo", gameUrl: "http://www.ifarchive.org/if-archive/games/hugo/ISH.hex", infoUrl: "https://ifdb.org/viewgame?id=1qv4dxqv5aj1894j"},
            {format: "Glulx", lang: "En", urlTitle:"Adventure", gameUrl: "https://ifarchive.org/if-archive/games/glulx/advent.ulx", infoUrl: "https://ifdb.org/viewgame?id=fft6pu91j85y4acv"},
            {format: "Adrift", lang: "En", urlTitle:"A Day at the Office", gameUrl: "http://www.ifarchive.org/if-archive/games/adrift/DayAtTheOffice.taf", infoUrl: "https://ifdb.org/viewgame?id=vfun9bzukewfb296"},
            {format: "Bocfel", lang: "en", urlTitle:"Photopia", gameUrl: "http://mirror.ifarchive.org/if-archive/games/zcode/photopia.z5", infoUrl: "https://ifdb.org/viewgame?id=ju778uv5xaswnlpl"},
            {format: "Bocfel", lang: "Es", urlTitle:"Fotopia", gameUrl: "http://mirror.ifarchive.org/if-archive/games/zcode/spanish/fotopia.z5", infoUrl: "https://ifdb.org/viewgame?id=ju778uv5xaswnlpl"},
            {format: "Glulx", lang: "Es", urlTitle:"La noche del ensayo", gameUrl: "http://www.ifarchive.org/if-archive/games/glulx/spanish/La_Noche_del_Ensayo.gblorb", infoUrl: "https://ifdb.org/viewgame?id=mxkm4l4flvzhj1ih"},
            {format: "AdvSys", lang: "En", urlTitle:"The Elf's Christmas Adventure", gameUrl: "http://mirror.ifarchive.org/if-archive/games/advsys/elves87.zip", infoUrl: "https://ifdb.org/viewgame?id=ctrwd117p4tvkpdh"}
        ])
    }

    SingleDownload {
        id: downloadGame

        property string downloadTitle: ""

        metadata: Metadata {
            showInIndicator: true
            title: downloadGame.downloadTitle
        }

        onFinished: {
            console.log("DOWLOADED GAME",path)
            var copiedFile = GelekBackend.storeGameFile(path.replace("file://",""), mainView.getNameFromUrl(path))

            mainPageStack.pop()
        }

        onErrorMessageChanged: {
            console.log("SingleDownload error", errorMessage)

            PopupUtils.open(
                errorDialog,
                gameDownloadPage,
                { "message" : errorMessage }
            )
        }
    }

    Component {
        id: downloadingDialog

        DownloadingDialog {
            onAbortDownload: downloadGame.cancel()
            percentage: downloadGame.progress
        }
    }

    Component {
        id: errorDialog

        ErrorDialog { }
    }
}

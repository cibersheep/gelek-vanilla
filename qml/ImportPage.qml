/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3

import "components"

Page {
    id: picker
    property var activeTransfer
    property bool allowMultipleFiles

    property var url
    property var handler
    property var contentType

    property var selectedItems: []

    signal close()
    signal accept(var fileUrls)

    header: BaseHeader {
        title: i18n.tr("Choose")
    }

    ContentPeerPicker {
        anchors { fill: parent; topMargin: picker.header.height }
        visible: picker.visible
        showTitle: false
        contentType: picker.contentType
        handler: picker.handler

        onPeerSelected: {
            peer.selectionType = picker.allowMultipleFiles
                ? ContentTransfer.Multiple
                : ContentTransfer.Single;

            picker.activeTransfer = peer.request();
            stateChangeConnection.target = picker.activeTransfer;
        }

        onCancelPressed: {
            close();
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            var statesForDebug = ["Created","Initiated","InProgress","Charged","Collected","Aborted","Finalized","Downloading","Downloaded"]
            console.log("DEBUG: activeTrasfer State is", statesForDebug[activeTransfer.state])

            switch (activeTransfer.state) {
                case ContentTransfer.Aborted:
                    console.log("Aborted")
                    break;
                case ContentTransfer.Collected:
                    console.log("Finalize and close")
                    activeTransfer.finalize();
                    close();
                    //If accept() is earlier, terp page is opened too early and fails. Then closes this Page
                    accept(picker.selectedItems);
                    break;
                case ContentTransfer.Finalized:
                    console.log("Finalized")
                    break;
                case ContentTransfer.InProgress:
                    console.log("InProgress")
                    break;
                case ContentTransfer.Charged:
                    for (var i in picker.activeTransfer.items) {
                        console.log("Move imported items",
                            //Move doesn't like file://
                            picker.activeTransfer.items[i].move(mainView.gamesURL.slice(0,-1).replace("file://",""))
                        )

                        //Move should updare the url but seem to falling back to copy does not
                        var gameName = String(picker.activeTransfer.items[i].url).split("/")
                        gameName = gameName[gameName.length -1]

                        picker.selectedItems.push({
                            "url": mainView.gamesURL + gameName,
                            "gameName": gameName
                        });
                        console.log("Url of the file to import", picker.selectedItems[i])
                    }
                    break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }

    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: picker.activeTransfer
    }
}

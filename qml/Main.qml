import QtQuick 2.9
import Lomiri.Components 1.3

import "components"
import Lomiri.Content 1.3
import Qt.labs.settings 1.0
import Lomiri.DownloadManager 1.2
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0
import Qt.labs.platform 1.0

import Qt.labs.folderlistmodel 2.1
import "js/gameFormatHelper.js" as Format
import "db/db.js" as DB
import GelekBackend 1.0

MainView {
    id: mainView
    objectName: "mainView"

    applicationName: "gelek-vanilla.cibersheep"
    width: units.gu(40)
    height: units.gu(75)

    signal extract(var file)

    //Margins
    property int marginColumn: units.gu(1)

    //Colors
    property string   lighterColor: LomiriColors.porcelain
    property bool        darkTheme: theme.name === "Lomiri.Components.Themes.SuruDark"
    property string selectionColor: darkTheme ? "#33949494" : "#335e5c59"
    property string     lightColor: darkTheme ? "#8c7f84" : "#75646a"
    property string      darkColor: darkTheme ? "#6a5a60" : "#4d4246"

    property string gameToAdd: ""
    property string currentBabelGame: ""
    property string gameFormat: ""
    property string gameIFID: ""
    property string gameCover: ""
    property string ifictionFile: ""
    //TODO: Use QtStandardPath
    property string baseRootURL: StandardPaths.writableLocation(StandardPaths.CacheLocation) + "/"
    property string gamesURL: baseRootURL + "Games/"
    property string savedGamesURL: baseRootURL + "SavedGames/"
    property string gamesFilesURL: baseRootURL + "GameFiles/"

    //Settings properties
    property var settings: Settings {
        property bool convergenceMode: true
        property bool pictureToTheRight: false
        property bool showMenuToTheRight: true
        property bool showPicturesInGame: true
        property bool dontForceFocus: false
        property string themeName: "Lomiri.Components.Themes.Ambiance"
        property bool haveCreatedFolders: false
        property bool debugging: false
        property bool oldSavedGameListExported: false

        onDebuggingChanged: GelekBackend.setDebuggingMode(settings.debugging);
    }

    property bool isLandscape: settings.convergenceMode && width > height + units.gu(10) // Consider the keyboard height

    signal themeChanged()

    anchorToKeyboard: true

    Component.onCompleted: {
        theme.name = settings.themeName;
        upgradeData();

        if (!settings.haveCreatedFolders) {
            GelekBackend.firtRunFolderSetUp();
            settings.haveCreatedFolders = true;
        }

        //Set debugging settings in cpp
        GelekBackend.setDebuggingMode(settings.debugging);

        mainPageStack.push(pageMain);
    }

    Connections {
        target: GelekBackend

        //let's get the answer from Babel
        onStdoutBabel: {
            if (settings.debugging) console.log("GAME from Babel is",game)
            currentBabelGame = game;

            //ToFix: We sould reset gameFormat
            var babelResult = GelekBackend.readBabelOut();
            Format.readBabelResult(babelResult);

            if (settings.debugging) console.log(
                "--------------------------\n",
                "Babel says: ", babelResult,
              "\nFormat is:  ", gameFormat,
              "\n--------------------------\n"
            )
        }
    }

    PageStack {
        id: mainPageStack
        anchors.fill: parent

        MainPage {
            id: pageMain
        }
    }

    //Add games to db
    function addGameToDB(game) {
        //Check if the game is in the db
        if (!DB.getGameInfo(game.name, game.id)) {
            DB.storeGameInfo(Qt.formatDateTime(new Date(), "dd-MM-yyyy, HH:mm:ss"), game.name, gameIFID, gameFormat, currentBabelGame)
        } else {
            console.log("TODO: Check if we need to update the game info")
        }
    }

    function getNameFromUrl(url) {
        var name = url.substr(url.lastIndexOf("/") + 1)

        return name
    }

    function prepareGameNamesAndOpen(importedGameToLaunch) {
        importedGameToLaunch = importedGameToLaunch.replace("file://","")
        //Find the format of the game with Babel
        GelekBackend.babel("-format", importedGameToLaunch)
        GelekBackend.babel("-ifid", importedGameToLaunch)
        console.log("Format in ImportPage is " + gameFormat, "IFID", gameIFID)
        var gameName = mainView.getNameFromUrl(importedGameToLaunch)

        var newGame = {
            "name": gameName,
            "format": gameFormat,
            "id": gameIFID,
            "url": importedGameToLaunch
        };

                mainView.addGameToDB(newGame);

        return newGame;
    }

    function runGame(filePath) {
        var saveGameData = DB.getGameByUrl(filePath);

        if (!saveGameData) {
            console.log("Game is not in the database");
            saveGameData = mainView.prepareGameNamesAndOpen(filePath);
        }

        gameFormat = saveGameData.format
        gameIFID = saveGameData.id

        Format.startGameByFormat(filePath, gameFormat, gameIFID)
    }

    function deleteGame(filePath) {
        GelekBackend.babel("-ifid", filePath)
        var gameRoute = filePath.split(/\//g)
        var folderURL = filePath.slice(0, filePath.indexOf(gameRoute[gameRoute.length -1]))
        ifictionFile = folderURL + gameIFID + ".ifiction"
        gameCover = folderURL + gameIFID
        GelekBackend.deleteSavedGames(ifictionFile)
        GelekBackend.deleteSavedGames(gameCover)
        GelekBackend.deleteSavedGames(filePath)
    }

    //Upgrading old database
    Component {
        id: importingMessage

        Rectangle {
            anchors.fill: parent
            color: "#eee"

            Text {
                text: i18n.tr("Importing List of Saved Games")
                wrapMode: Text.WordWrap
                horizontalAlignment: Text.AlignHCenter
                width: parent.width

                anchors {
                    horizontalCenter: activity.horizontalCenter
                    bottom: activity.top
                    bottomMargin: units.gu(4)
                }
            }

            ActivityIndicator {
                id: activity
                anchors.centerIn: parent
                running: true
            }
        }
    }

    Loader {
        id: busyLoader
        anchors.fill: parent
    }

    function upgradeData() {
        if (!settings.oldSavedGameListExported) {
            busyLoader.sourceComponent = importingMessage;
            console.log("Exporting old saveGameList");
            var listOfSavedGames = GelekBackend.exportSave();
            if (listOfSavedGames != null) {
                listOfSavedGames = listOfSavedGames.toString().trim().split("},");

                for (var i=0; i < listOfSavedGames.length; i++) {
                    if (listOfSavedGames[i].toString() == "") {
                        continue;
                    }

                    var name = listOfSavedGames[i].toString().split(": { ")[0].replace(/\"/g,"");
                    var info = listOfSavedGames[i].toString().split(": { ")[1].replace(/\"/g,"");
                    var game = info.slice(5,info.indexOf(","));
                    var time = info.slice(info.indexOf("time:") + 5).trim();

                    if (settings.debugging) console.log("savedGame " + i + " info " + info + "\n time " + time);
                    DB.storeSavedGame(time.toString(), game.toString(), "", name.toString());
                }
            }

            settings.oldSavedGameListExported = true;
            busyLoader.sourceComponent = undefined;
        }
    }
}

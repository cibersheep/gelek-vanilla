import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.3

import "components"
import Qt.labs.folderlistmodel 2.1

import UnzipTools 1.0
import GelekBackend 1.0
import "js/gameFormatHelper.js" as Format

Page {
    id: mainPage
    anchors.fill: parent

    header: MainHeader {
        id: pageHeader
        title: i18n.tr("Gelek Vanilla")
        flickable: flickable

        onSearchText: sortedImportedGamesModel.filter.pattern = new RegExp(typedText, "i")
    }

    Component.onCompleted: {
        //Fixes: No games on the list after first import
        importedGamesModel.rootFolder = gamesURL
        importedGamesModel.folder = gamesURL
    }

    Flickable {
        id: flickable
        clip: true

        anchors {
            top: pageHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        contentHeight: pageHeader.isSearching
            ? contentItem.childrenRect.height
            : contentItem.childrenRect.height + units.gu(2)

        Column {
            id: addGame
            width: parent.width
            visible: !pageHeader.isSearching

            anchors {
                top: parent.top
                topMargin: units.gu(2)
            }

            spacing: units.gu(1)

            Icon {
                id: importGame
                width: units.gu(6)
                height: width
                anchors.horizontalCenter: parent.horizontalCenter
                name: "add"
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: i18n.tr("Import")
                color: theme.palette.normal.backgroundText
                textSize: Label.XLarge
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.backgroundText
                text: i18n.tr("a new game")
            }

            Text {
                text: i18n.tr("Accepted formats: %1").arg(
                    "adrift (3, 4), agt, advsys, glulx, hugo, " +
                    "level9, magnetic scrolls, scott adams, tads, zcode"
                )

                width: parent.width - marginColumn * 12
                anchors.horizontalCenter: parent.horizontalCenter
                horizontalAlignment: Text.AlignHCenter
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: theme.palette.normal.backgroundTertiaryText
            }
        }

        MouseArea {
            anchors.fill: addGame
            visible: addGame.visible
            enabled: visible

            onClicked: requestImport()
        }

        Column {
            id: previousGames
            width: parent.width

            anchors {
                horizontalCenter: parent.horizontalCenter
                top: addGame.visible ? addGame.bottom : parent.top
                topMargin: addGame.visible ? units.gu(10) : 0
            }

            Label {
                height: units.gu(4)
                anchors.horizontalCenter: parent.horizontalCenter
                text: importedGamesModel.count === 0 ? i18n.tr("No Imported Games Yet") : i18n.tr("Previouly Imported Games")
                color: lightColor
                visible: addGame.visible
            }

            FolderListModel {
                id: importedGamesModel
                rootFolder: gamesURL
                folder: gamesURL
                nameFilters: [
                    "*.z*", "*.sna", "*.dat", "*.tap", "*.l9", "*.st", "*.atr", "*.com", "*.bin", "*.tzx",
                    "*.mag", "*.ulx", "*.gblorb", "*.gam", "*.blb", "*.t3", "*.agx", "*.d$$", "*.taf", "*.hex",
                    "*.a3c", "*.acd",
                    "*.Z*", "*.SNA", "*.DAT", "*.TAP", "*.L9", "*.ST", "*.ATR", "*.COM", "*.BIN", "*.TZX",
                    "*.MAG", "*.ULX", "*.GBLORB", "*.GAM", "*.BLB", "*.T3", "*.AGX", "*.D$$", "*.TAF", "*.HEX",
                    "*.A3C", "*.ACD"
                ]
                showHidden: true
                showDirs: false
                sortField: FolderListModel.Time
            }

            GameListView {
                id: importedGamesDelegate
            }

            SortFilterModel {
                id: sortedImportedGamesModel
                model: importedGamesModel
                sort.property: "fileAccessed"
                sort.order: Qt.DescendingOrder
                // case insensitive sorting
                sortCaseSensitivity: Qt.CaseInsensitive
                filterCaseSensitivity: Qt.CaseInsensitive
                filter.property: "fileName"
                filter.pattern: /\S+/
            }

            ListView {
                id: importGameList
                interactive: false
                width: parent.width

                anchors.horizontalCenter: parent.horizontalCenter
                height: childrenRect.height

                model: sortedImportedGamesModel
                delegate: importedGamesDelegate
            }
        }

        Column {
            id: otherOptions
            width: parent.width

            anchors {
                top: previousGames.bottom
                leftMargin: marginColumn
                rightMargin: marginColumn
                topMargin: units.gu(6)
            }

            Divider { }

            ListItem {
                width: parent.width
                divider.visible: true

                GelekItemLayout {
                    title.text: i18n.tr("Saved Games")
                }

                onClicked: {
                    pageHeader.closeHeader()
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("SavedGamesPage.qml"));
                }
            }

            ListItem {
                width: parent.width
                divider.visible: true

                GelekItemLayout {
                    title.text: i18n.tr("Download Games")
                }

                onClicked: {
                    pageHeader.closeHeader()
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("DownloadsPage.qml"));
                }
            }
        }
    }

    Component {
        id: extractingDialog

        ExtractingDialog { }
    }

    function requestImport() {
        var importPage =  mainPageStack.push(
            Qt.resolvedUrl("ImportPage.qml"),
            {   "contentType": ContentType.All,
                "handler": ContentHandler.Source,
                "allowMultipleFiles":  true
            }
        );

        importPage.close.connect(function() {
            mainPageStack.pop();
        });

        importPage.accept.connect(function(fileUrls) {
            if (settings.debugging) {
                for (var i= 0; i< fileUrls.length; i++) {
                    console.log("DEBUG: Url",i,"Try to parse",fileUrls[i]);
                    console.log("gameName:", fileUrls[i].gameName, "gameToAdd", fileUrls[i].url)
                }
            }

            playImportedGames(fileUrls)
        });
    }

    function playImportedGames(fileUrls) {
        //Choose the game to run from list or run if only 1
        if (fileUrls.length > 1) {
            var gameChooser = PopupUtils.open(
                Qt.resolvedUrl("components/MultipleImportsPopup.qml"),
                null,
                {"filesModel": fileUrls}
            );

            gameChooser.close.connect(function() {
                PopupUtils.close(gameChooser);
            });

            gameChooser.chosen.connect(function(gameChosen) {
                prepareGameNamesAndOpen(gameChosen);
            });
        } else {
            //Get the updated url of the game

            /* Needs to be done in a function apart (not in playImportedGames)
             * or gameChooser close after this
             */
            mainView.prepareGameNamesAndOpen(fileUrls[0].url)
        }
    }

    function extractArchive(filePath) {
        var parentDirectory = filePath.substring(0, filePath.lastIndexOf("/") + 1)
        var fileName = filePath.substring(filePath.lastIndexOf("/") + 1)

        PopupUtils.open(extractingDialog, mainPage, { "fileName" : fileName })
        UnzipTools.extractZip(fileName, parentDirectory, parentDirectory)
    }

    Connections {
        target: mainView

        onExtract: extractArchive(file)
    }
}

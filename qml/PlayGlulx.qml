import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import "components"
import "components/CommonActions"
import "js/jsonParser.js" as JsParser

import GelekBackend 1.0

//TODO:
//Refresh window width on glk intead of resizing font pixel
//Start Wonder and type E

Page {
    id: playGlulx

    property string game
    property string gameName:""
    property string completeOut
    property var    response
    property bool   bottomEdgeVisible: false

    property int    canvasWidth: 324
    property int    canvasHeight: 0
    property alias  img: gameImage.img

    property bool   renderingImage: false
    property var    commandsBuffer: []
    property int    oldGeneration: 0
    readonly property int untilTheEnd: -1
    property bool   showNextScroll: false
    property bool   checkFocus: true

    property int viewboxw: -1
    property int viewboxh: -1
    property int clipWidth: -1
    property int clipHeight: -1

    property int windRoseWidth: units.gu(4)
    property int screenWidth: 80
    property int gridHeight: 0

    anchors.fill: parent

    header: GelekHeader {
        id: gameHeader
        title: i18n.tr("Glulx Interpreter")

        //Avoid OSK to keep visible when tapping on a header's page
        onKeepCheckingFocus: checkFocus = isChecking
    }

    Component.onCompleted: {
        if (settings.debugging) console.log("The ID is: " + gameIFID)
        GelekBackend.extractBlorb(game, gameIFID)
        GelekBackend.launchTerp("glulxeremglk", game)
        GelekBackend.sendCommand('{ "type": "init", "gen": 0, "metrics": { "width":1000, "height":800, "charwidth":12, "charheight":14 },  "support": [ "graphics", "graphicswin", "timer" ] }')
        command.focus = true
    }

    Connections {
        target: GelekBackend

        //let's get the answer from GLK
        onStdoutLevel:{
            response = ""
            var stdoutResponse = GelekBackend.readSOL()

            while (stdoutResponse.indexOf("{\"type\":\"error\",") !== -1) {
                var endOfError = stdoutResponse.indexOf("}");
                console.log("GLK error: " + stdoutResponse.slice(0,endOfError+1).replace(/\n/g, ""));
                stdoutResponse = stdoutResponse.replace(/{\"type\":\"error\",.*}/i, "");
            }

            //Check if we got a complete json from stdout
            completeOut += stdoutResponse
            response = JsParser.isJsonFinished(completeOut)

            if (response) {
                if (settings.debugging) console.log("We have a json");
                //DEBUG:
                if (settings.debugging) console.log("| json stdout -------------------\n" + completeOut + "\n----------------------");
                response = JSON.parse(completeOut);
                completeOut = "";
                JsParser.formatText(response);
            } else
                if (settings.debugging) console.log("Not a json. Waiting for next info");
        }

        //let's get the answer from Babel
        onStdoutBabel: {
            var babelResult = GelekBackend.readBabelOut()
        }

        onStdoutBlorb: if (settings.debugging) console.log("Blorb debbug:",GelekBackend.readBlorbOut())
    }

    GamePicture {
        id: gameImage
    }

    Flickable {
        id: gameFlickable

        property real previousContentHeight: 0
        property real scrollLeft: 0

        clip: true
        contentHeight: txt.height + units.gu(4)
        width: isLandscape && (gameImage.visible || commonsActionsColumnL9.visible)
            ? parent.width * 0.5
            : parent.width

        anchors {
            //Flickable should anchor at the bottom of the image except that is at the right
            top: gameImage.visible && (!isLandscape || !settings.pictureToTheRight)
                ? gameImage.bottom
                : gridLinesRec.bottom

            //topMargin: frotzHeader.height + gridLinesRec.height + units.gu(2)
            bottom: inputArea.top
            bottomMargin: units.gu(1)
        }

        Text {
            id: txt
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            textFormat: Text.RichText
            horizontalAlignment: Text.AlignJustify
            color: Theme.palette.normal.backgroundText

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                margins: units.gu(2)
            }
        }

        onFlickStarted: if (showNextScroll) {
            showNextScroll = false;
            scrollLeft = 0;
        }
    }

    Rectangle {
        id: gridLinesRec
        anchors.top: parent.top
        anchors.topMargin: gameHeader.visible ? gameHeader.height : 0
        height: gridLines.height
        width: gameFlickable.width

        ListView {
            id: gridLines
            interactive: false
            width: parent.width
            height: units.gu(3) * gridHeight

            model: gridModel
            delegate: gridDelegate
        }
    }

    HideHeader {
        anchors.rightMargin: commonsActionsColumnL9.visible
            ?  commonsActionsColumnL9.width + units.gu(2)
            : units.gu(2)
    }

    MouseArea {
        //Make sure we capture taps on the inputArea area
        anchors {
            bottom: inputArea.bottom
            left: parent.left
            right: parent.right
        }

        height: inputArea.height + units.gu(1)

        onReleased: command.focus = true;
    }

    Row {
        id: inputArea

        property bool extKeybVisible: false

        spacing: units.gu(1)
        width: gameFlickable.width - instruction.width

        anchors {
            left: parent.left
            right: isLandscape && (gameImage.visible || commonsActionsColumnL9.visible)
                ? undefined
                : parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }

        Button {
            id: extKeys
            width: units.gu(4)
            height: width
            color: darkColor
            opacity: enabled ? 1 : .4
            //Make sure the touch area is big enough
            sensingMargins.all: units.gu(2)

            Icon {
                width: units.gu(2)
                anchors.centerIn: parent
                color: lighterColor
                name: inputArea.extKeybVisible
                    ? "toolkit_chevron-down_2gu"
                    : "toolkit_chevron-up_2gu"
            }

            onClicked: inputArea.extKeybVisible = !inputArea.extKeybVisible
        }

        TextField {
            id: command
            width: parent.width - instruction.width - extKeys.width - units.gu(2)
            onTextChanged: if (text.length === 1 && response.input[0].type === "char") accepted()

            onAccepted: {
                command.focus=false;
                //command.focus=true; //Let the onFocusChanged take care of this

                if (!showNextScroll) {
                    JsParser.commandToBuffer(command.text);
                    command.text = "";
                } else {
                    JsParser.flickText(gameFlickable.scrollLeft)
                }
            }

            onFocusChanged: {
                if (!command.focus && checkFocus && !bottomEdgeVisible) {
                    command.focus = true
                }
            }

            Keys.onPressed: {
                if (response.input && response.input[0].type === "char") {
                    event.accepted = JsParser.keyPressedCheck(event.key);
                }
            }
        }

        Button {
            id: instruction
            width: units.gu(4)
            height: width
            color: darkColor
            opacity: enabled ? 1 : .4
            //Make sure the touch area is big enough
            sensingMargins.all: units.gu(2)

            enabled: {
                if (command.text !== "" || showNextScroll || (response.input[0] && response.input[0].type === "char")) {
                    return true
                } else {
                    return false
                }
            }

            Icon {
                width: units.gu(2)
                anchors.centerIn: parent
                color: instruction.enabled
                    ? lighterColor
                    : Theme.palette.disabled.foregroundText
                name: showNextScroll ? "add" : "keyboard-enter"
            }

            onClicked: command.accepted()
        }
    }

    ExtendedKeyboard {
        id: extededKeyboard
        visible: inputArea.extKeybVisible

        anchors {
            horizontalCenter: inputArea.horizontalCenter
            bottom: inputArea.top
        }

        function processChar(character) {
            if (response.input[0].type === "char") {
                JsParser.commandToBuffer(character);
            }
        }
    }

    BottomEdgeMenuL9 {
        id: bottomEdge

        onCollapseCompleted: {
            command.focus = true
            delayedFlickText.start()
        }
    }

    Connections {
        target: mainView

        onThemeChanged: {
            //HACK: Dirty hack to avoid bottomEdge stops working after changing themes
            bottomEdge.commit()
            bottomEdge.collapse()
        }
    }

    Component {
        id: showPopUpSave
        SavePopup { }
    }

    Component {
        id: showPopUpRestore
        RestorePopup { }
    }

    //This element is the grid = every line is a Row of Text elements (to be able to mix formats and colors)
    Component {
        id: gridDelegate

        Row {
            id: lineDelegate
            width: parent.width
            height: units.gu(1.7 * 12 * gameFlickable.width / 1000 / units.gu(1)) //units.gu(2) //gridTxt.height
            clip: true

            Repeater {
                model: singleRow

                Text {
                    id: gridTxt
                    text: model.text
                    color: model.color
                    font.bold: model.bold
                    font.italic: model.italic
                    verticalAlignment: Text.AlignVCenter

                    height: parent.height
                    font.family: "monospace"
                    //textFormat: Text.PlainText
                    fontSizeMode: Text.FixedSize
                    font.pixelSize: units.gu(1.7 * 12 * gameFlickable.width / 1000 / units.gu(1)) //units.dp(1.75 * (width / model.text.length))

                    Rectangle {
                        z: -1
                        anchors.fill: gridTxt
                        width: gridTxt.width
                        color: model.bgcolor
                    }
                }
            }
        }
    }

    ListModel {
        id: gridModel

        property var singleRow: []

        function initialize() {
            gridModel.clear();
        }
    }

    Connections {
        target: Qt.inputMethod

        onVisibleChanged: delayedFlickText.start()
    }

    Timer {
        id: delayedFlickText
        interval: 120
        onTriggered: JsParser.flickText(untilTheEnd)
    }

    Timer {
        id: retrieveTimer
        repeat: true

        onTriggered: {
            if (response.gen) {
                //Process on command in the commandBuffer
                JsParser.processBuffer()
            } else if (settings.debugging) console.log("retrieveTimer triggered but we have no response.gen. We are doing nothing")
        }
    }

    CommonActionsColumn {
        id: commonsActionsColumnL9
        visible: isLandscape && settings.showMenuToTheRight

        anchors {
            top: gameImage.visible && settings.pictureToTheRight
                ? gameImage.bottom
                : gameHeader.visible
                    ? gameHeader.bottom
                    : parent.top
            left: gameFlickable.right
            right: parent.right
        }
    }

    Component.onDestruction: JsParser.terminateTerp();
}

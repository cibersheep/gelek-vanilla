import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Qt.labs.folderlistmodel 2.1

import "components"
import "components/CommonActions"
import GelekBackend 1.0

Page {
    id: playLevel9

    property string game
    property string gameName: ""

    property int    oldGeneration: 0
    property string completeOut
    property var    response
    property int    canvasWidth: 324
    property int    canvasHeight: 225
    property var    commandsBuffer: []
    readonly property int untilTheEnd: -1

    property bool   bottomEdgeVisible: false
    property bool   renderingImage: false
    property bool   showNextScroll: false
    property bool   checkFocus: true
    property string currentPalette: "ubuntu"

    property int viewboxw: -1
    property int viewboxh: -1
    property int clipWidth: -1
    property int clipHeight: -1

    property int windRoseWidth: units.gu(4)

    anchors.fill: parent

    header: GelekHeader {
        id: gameHeader
        title: i18n.tr("Level 9 interpreter")

        //Avoid OSK to keep visible when tapping on a header's page
        onKeepCheckingFocus: checkFocus = isChecking
    }

    Component.onCompleted: {
        GelekBackend.launchTerp("level9remglk", game)
        GelekBackend.sendCommand('{ "type": "init", "gen": 0, "metrics": { "width":800, "height":600 },  "support": [ "graphics", "graphicswin", "timer" ] }')
        command.focus = true
    }

    Connections {
        target: GelekBackend

        onStdoutLevel:{
            response = ""
            var stdoutResponse = GelekBackend.readSOL()

            while (stdoutResponse.indexOf("{\"type\":\"error\",") !== -1) {
                var endOfError = stdoutResponse.indexOf("}");
                console.log("GLK error: " + stdoutResponse.slice(0,endOfError+1).replace(/\n/g, ""));
                stdoutResponse = stdoutResponse.replace(/{\"type\":\"error\",.*}/i, "");
            }

            //Check if we got a complete json from stdout
            completeOut += stdoutResponse
            response = isJsonFinished(completeOut)

            //response is a json or false
            if (response) {
                if (settings.debugging) console.log(
                    "| json stdout -------------------\n",
                    completeOut,
                  "\n| -------------------------------"
                );

                completeOut = ""
                formatText(response)
            } else {
                if (settings.debugging) console.log("Not a json. Waiting for next info")
            }
        }
    }

    LomiriShape {
        id: gameImage
        visible: settings.showPicturesInGame && viewboxw !== -1
        width: isLandscape
            ? settings.pictureToTheRight
                ? Math.min(Math.max(clipWidth, (parent.width  - units.gu(6)) * .4), (parent.height -units.gu(10)) / 3  * (clipWidth/clipHeight) + 6)
                : Math.min(Math.max(clipWidth, (parent.width  - units.gu(6)) * .6), (parent.height -units.gu(10)) / 3  * (clipWidth/clipHeight) + 6)
            : Math.min(Math.max(clipWidth, parent.width - units.gu(2)), (parent.height -units.gu(10))/ 3 * (clipWidth/clipHeight) + 6)
        height: width * (clipHeight/clipWidth) + 6
        backgroundColor: Theme.palette.normal.background

        /*
         * This would make the image to be wider as the text
           width: txt.width
           height: width* (img.height / img.width) -6
         */

        aspect: LomiriShape.Flat

        anchors {
            horizontalCenter: settings.pictureToTheRight && isLandscape
                ? commonsActionsColumnL9.horizontalCenter
                : gameFlickable.horizontalCenter
            top: gameHeader.visible
                ? gameHeader.bottom
                : parent.top
        }

        sourceFillMode: LomiriShape.PreserveAspectCrop
        sourceVerticalAlignment: LomiriShape.AlignVCenter
        sourceHorizontalAlignment: LomiriShape.AlignHCenter
        sourceScale: Qt.vector2d(Math.min(canvasWidth / clipWidth, canvasHeight / clipHeight), Math.min(canvasWidth / clipWidth, canvasHeight / clipHeight) )

        source: Canvas {
            id: img
            width: canvasWidth
            height: canvasHeight
            //Deprecated canvasWindow
            //canvasWindow: Qt.rect(viewboxw, viewboxh, clipWidth, clipHeight)
        }
    }

    Flickable {
        id: gameFlickable

        property real previousContentHeight: 0
        property real scrollLeft: 0

        clip: true
        contentHeight: txt.height + units.gu(4)

        width: isLandscape && (commonsActionsColumnL9.visible || (settings.pictureToTheRight && gameImage.visible))
            ? parent.width * 0.6
            : parent.width

        anchors {
            //Flickable should anchor at the bottom of the image except that is at the right
            top: gameImage.visible && (!isLandscape || !settings.pictureToTheRight)
                ? gameImage.bottom
                : gameHeader.visible
                    ? gameHeader.bottom
                    : parent.top

            bottom: inputArea.top
            bottomMargin: units.gu(1)
        }

        Text {
            id: txt
            wrapMode: Text.WrapAtWordBoundaryOrAnywhere
            textFormat: Text.RichText
            horizontalAlignment: Text.AlignJustify
            color: Theme.palette.normal.backgroundText

            anchors {
                left: parent.left
                right: parent.right
                top: parent.top
                margins: units.gu(2)
            }
        }

        onFlickStarted: if (showNextScroll) {
            showNextScroll = false;
            scrollLeft = 0;
        }
    }

    HideHeader {
        anchors.rightMargin: commonsActionsColumnL9.visible
            ?  commonsActionsColumnL9.width + units.gu(2)
            : units.gu(2)
    }

    MouseArea {
        //Make sure we capture taps on the inputArea area
        anchors {
            bottom: inputArea.bottom
            left: parent.left
            right: parent.right
        }

        height: inputArea.height + units.gu(1)

        onReleased: command.focus = true;
    }

    Row {
        id: inputArea
        spacing: units.gu(1)
        width: gameFlickable.width - instruction.width

        anchors {
            left: parent.left
            right: isLandscape && (gameImage.visible || commonsActionsColumnL9.visible)
                ? undefined
                : parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }

        TextField {
            id: command
            width: parent.width - instruction.width

            //inputMethodHints: Qt.ImhNoPredictiveText //https://gitlab.com/cibersheep/gelek-vanilla/-/issues/33
            //If this--^ property is defined, onAccepted() will make the rect flick and everything move

            onTextChanged: if (text.length === 1 && response.input[0].type === "char") accepted()

            onAccepted: {
                command.focus = false;
                //command.focus=true; //Let the onFocusChanged take care of this

                if (!showNextScroll) {
                    commandToBuffer(command.text);
                    command.text = "";
                } else {
                    flickText(gameFlickable.scrollLeft)
                }
            }

            onFocusChanged: {
                if (!command.focus && checkFocus && !bottomEdgeVisible) {
                    command.focus = true
                }
            }
        }

        Button {
            id: instruction
            width: units.gu(4)
            height: width
            enabled: command.text !== "" || showNextScroll || (response && response.input[0].type === "char")
            color: darkColor
            opacity: enabled ? 1 : .4
            //Make sure the touch area is big enough
            sensingMargins.all: units.gu(2)

            Icon {
                width: units.gu(2)
                anchors.centerIn: parent
                color: instruction.enabled
                    ? lighterColor
                    : Theme.palette.disabled.foregroundText
                name: showNextScroll ? "add" : "keyboard-enter"
            }

            onClicked: command.accepted()
        }
    }

    BottomEdgeMenuL9 {
        id: bottomEdge

        onCollapseCompleted: {
            command.focus = true
            delayedFlickText.start()
        }
    }

    CommonActionsColumn {
        id: commonsActionsColumnL9
        visible: isLandscape && settings.showMenuToTheRight
        height: parent.height / 3

        anchors {
            top: gameImage.visible && settings.pictureToTheRight
                ? gameImage.bottom
                : gameHeader.visible
                    ? gameHeader.bottom
                    : parent.top
            left: gameFlickable.right
            right: parent.right
        }
    }

    Connections {
        target: mainView

        onThemeChanged: {
            //HACK: Dirty hack to avoid bottomEdge stops working after changing themes
            bottomEdge.commit()
            bottomEdge.collapse()
        }
    }

    function commandToBuffer(command) {
        //Add the latest command to the buffer
        commandsBuffer.push(command);

        //Make sure that if we don't have a timer, to send the last command to GLK
        if (!retrieveTimer.running) {
            sendCommandToGtk(commandsBuffer.shift());
        }
    }

    function formatText(json) {
        gameFlickable.previousContentHeight = gameFlickable.contentHeight

        if (settings.debugging) console.log("formatText")
        if (json.content) {
            json.content.forEach(function(x){
                if (x.hasOwnProperty("text")) {
                    for (var i = 0; i <= x.text.length -1; i++) {
                        var jsonContent = x.text[i].content

                        if (jsonContent) {
                            for (var j = 0; j <= jsonContent.length -1; j++) {
                                jsonContent[j].text = jsonContent[j].text.replace("&","&amp;")
                                if (settings.debugging) console.log(jsonContent[j].text)

                                switch (jsonContent[j].style) {
                                    case "header":
                                        txt.text += "<font size='+2' color='" + darkColor + "'><b>" + jsonContent[j].text + "</b></font><br/><br/>"
                                        if (settings.debugging) console.log(txt.text)
                                        break
                                    case "subheader":
                                        txt.text += "<font size='+1' color='" + lightColor +"'><b><i>" + jsonContent[j].text + "</i></b></font><br/>"
                                        break
                                    case "emphasized":
                                        txt.text += "<i>" + jsonContent[j].text + "</i>"
                                        break
                                    case "input":
                                        txt.text += "<i>" + jsonContent[j].text + "</i><br/>"
                                        break
                                    default:
                                        if (jsonContent[j].text !== "> ") { //Hides the "> ". We don't need it in Gelek
                                            if (j < jsonContent.length -1) {
                                                txt.text += jsonContent[j].text
                                            }
                                            else txt.text += jsonContent[j].text  + "<br/>"
                                        }
                                        if (gameName === "") getNameYear(jsonContent[j].text);
                                }
                            }
                        }
                        else {
                            if (settings.debugging) console.log("Empty line")
                            txt.text += "<br/>"
                        }
                    }

                    var textHeightToUpdate = gameFlickable.contentHeight - gameFlickable.previousContentHeight;

                    //TODO: Make the flick accordinly to the space left in the screen
                    //while (!gameFlickable.atYEnd) {gameFlickable.contentY += units.gu(1)}
                    flickText(textHeightToUpdate);
                }

                if (x.hasOwnProperty("draw")) {
                    if (settings.debugging) console.log("We have an image")
                    var xDraw;
                    var yDraw;
                    var wDraw;
                    var hDraw;

                    for (var j=0; j<x.draw.length; j++){
                        xDraw = 0;
                        yDraw = 0;
                        wDraw = canvasWidth;
                        hDraw = canvasHeight;
                        var ctx = img.getContext("2d");
                        ctx.fillStyle = palette(x.draw[j].color)

                        if (x.draw[j].special == "fill") {
                            if (x.draw[j].hasOwnProperty("x")) {
                                xDraw = x.draw[j].x;
                                wDraw = x.draw[j].width;
                            }

                            if (x.draw[j].hasOwnProperty("y")) {
                                yDraw = x.draw[j].y;
                                hDraw = x.draw[j].height;
                            }

                            //The draw[1] must be the size of the image
                            if (j == 2 && viewboxw == -1) {
                                if (settings.debugging) console.log("j 1 rectangle (should be image size):",wDraw,hDraw)
                                clipWidth = wDraw;
                                clipHeight = hDraw
                                viewboxw = xDraw
                                viewboxh = yDraw
                                if (settings.debugging) console.log("j 1",viewboxw,viewboxh)
                                // Set the clipping area
                            }

                            ctx.fillRect(xDraw, yDraw, wDraw, hDraw);
                            img.requestPaint()
                        }
                    }
                }

            }) //forEach
        }

        /* Mole and Archers sagas, if played with graphics
         * after selection an option, the game waits for
         * timer updates.
         * It supposed to set a repeating timer. In this way: input
         * from player should go into a pile to avoid conflicting
         * gen number...
         */
        if (json.timer) {
            if (settings.debugging) console.log("Timer interval set to: " + json.timer)
            retrieveTimer.interval = json.timer
            retrieveTimer.start()
        }

        if (json.timer === null) {
            retrieveTimer.stop()
        }

        if (json.windows) {
            if (settings.debugging) console.log("Found windows. Get w and h graphics")

            json.windows.forEach(function(x){
                if (x.graphwidth) { canvasWidth = x.graphwidth;}
                if (x.graphheight) { canvasHeight = x.graphheight;}
            })
        }

        if (json.specialinput) {
            if (json.specialinput.type === "fileref_prompt") {
                GelekBackend.createSavedGamesDir()

                switch (json.specialinput.filemode) {
                    case "read":
                        PopupUtils.open(showPopUpRestore)
                        break
                    case "write":
                        PopupUtils.open(showPopUpSave)
                        break
                }
            }
        }

        //If there's more commands in the buffer and we have no timer running
        //take the next command
        if (commandsBuffer.length > 0 && !retrieveTimer.running) {
            sendCommandToGtk(commandsBuffer.shift());
        }
    }

    function palette(color) {
        if (currentPalette == "original") {
            return color;
        }
        //             [ blue,     Maroon,    Dark blue, Red,       Yellow,    Green (D),  White,     Black]
        var palette = {
             "ubuntu": ["#bef2ff", "#c44114", "#19b6ee", "#ed3146", "#f5d412", "#3eb34f", "#fdfdfd", "#111111"],
              "green": ["#00ee00", "#005700", "#00AC00", "#002C00", "#00C900", "#00DC00", "#000000", "#ededed"],
            "amstrad": ["#10FFFE", "#FF00FC", "#2200FB", "#FF001B", "#FEFF39", "#008019", "#FFFFFF", "#000000"]
        }
        switch (color) {
            case "#AFEEEE": //blue
                return palette[currentPalette][0];
                break
            case "#8B5742": //Maroon
                return palette[currentPalette][1];
                break
            case "#5CACEE": //dark blue
                return palette[currentPalette][2];
                break
            case "#EE2C2C": //red
                return palette[currentPalette][3];
                break
            case "#EEC900": //yellow
                return palette[currentPalette][4];
                break
            case "#43DC80": //green
                return palette[currentPalette][5];
                break
            case "#FFFFFF": //white
                return palette[currentPalette][6];
                break
            case "#000000": //black
                return palette[currentPalette][7];
                break
            default:
                return color
        }
    }

    function multiWords(s,t){
        var a = t.toLowerCase();

        for(var i=0; i<s.length; i++){
            if (a.indexOf(s[i].toLowerCase()) != -1){
                if (settings.debugging) console.log('Game Name Found in the List: ' + s[i]);
                return s[i];
            } else {
                if (settings.debugging) console.log('Game Name Not Found in the List');
            }
        }

        return "";
    }

    //A bit too hacky. Make a better way of detect game. Use package info?
    function getNameYear(text) {
        //Check if glk has already detected the game
        if (text.match(/\[/)) {
            gameName = text.replace(/\[|\]/g,"").trim();
        } else {
            //Find the name of the game in the intro text
            gameName = multiWords(["Emerald Isle", "Secret Diary of Adrian Mole", "The Archers", "Erik the Viking", "Dungeon Adventure", "Adventure Quest", "Snowball", "Return to Eden", "Worm in Paradise", "Knight Orc", "Gnome Ranger", "Lords of Time", "Red Moon", "Price of Magik", "Lancelot", "Ingrid's Back", "Scapeghost"],text);
            if (text.match(/\d+/g)) {
                var numbers = text.match(/\d+/g).map(Number);
                for (var j = 0; j < numbers.length; j++) {
                    if (numbers[j] > 1900) {
                        gameName += " " + numbers[j];
                        break;
                    }
                }
            }
        }

        if (settings.debugging) console.log("Debug: gameName = " + gameName);
    }

    function sendCommandToGtk(commandString) {
        //Dirty hack to never run into wrong generation number
        //if Timer goes too quick
        if (response.gen !== oldGeneration) {
            if (response.input[0].type === "char") { //Make char input accepted from TextField
                var character

                if (commandString == "") {
                    //If we accept the text, is a return
                    character = "return"
                } else {
                    //Make sure we send only the first letter
                    character = commandString.slice(0,1)
                }

                if (settings.debugging) console.log("Sending command. Char type --------------------------------------")
                GelekBackend.sendCommand('{ "type":"char", "gen":' + response.gen + ', "window":' + response.input[0].id + ', "value": "' + character + '" }')
                if (settings.debugging) console.log("+---- Gen: " + response.input[0].gen + " : " + response.gen + " : "+ oldGeneration)

            } else if (commandString !== "") {
                GelekBackend.sendCommand('{ "type":"line", "gen":' + response.gen + ', "window":' + response.input[0].id + ', "value": "' + commandString + '" }') //Should be command.text.slice(response.input[0].maxlen)
                commandString = ""
            }
        }

        command.focus = true
    }

    //Save
    Component {
        id: showPopUpSave
        SavePopup {}
    }

    //Restore
    Component {
        id: showPopUpRestore
        RestorePopup {}
    }

    function flickText(upToHere) {
        if (upToHere === untilTheEnd || upToHere <= gameFlickable.height) {
            while (!gameFlickable.atYEnd) {
                gameFlickable.contentY += units.dp(1);
            }

            showNextScroll = false;
            gameFlickable.scrollLeft = -1
        } else {
            gameFlickable.scrollLeft = upToHere - gameFlickable.height;
            upToHere = gameFlickable.height;
            showNextScroll = true
            upToHere = upToHere / units.dp(1);

            for (var i = 0; i <= upToHere; i++) {
                if (gameFlickable.atYEnd) {
                    showNextScroll = false;
                    break;
                }

                gameFlickable.contentY += units.dp(1);
            }
        }
    }

    //Return json or false
    function isJsonFinished(str) {
        try {
            var jsonparesed = JSON.parse(str);
        } catch (e) {
            return false;
        }

        return jsonparesed;
    }

    Connections {
        target: Qt.inputMethod

        onVisibleChanged: {
            command.focus = true
            delayedFlickText.start()
        }
    }

    Timer {
        id: delayedFlickText
        interval: 120
        onTriggered: flickText(untilTheEnd)
    }


    Timer {
        id: retrieveTimer
        repeat: true

        onTriggered: {
            if (response.gen) {
                if (commandsBuffer.length > 0) {
                    sendCommandToGtk(commandsBuffer.shift());
                    commandsBuffer = [];
                    if (settings.debugging) console.log("Emptying buffer")
                } else {
                    //Dirty hack to never run into wrong generation number
                    //because Timer goes too quick
                    if (oldGeneration !== response.gen) {
                        GelekBackend.sendCommand('{ "type":"timer", "gen":' + response.gen + ' }');
                    }
                }

                oldGeneration = response.gen
            }
        }
    }

    Component.onDestruction: GelekBackend.stopTerp();
}

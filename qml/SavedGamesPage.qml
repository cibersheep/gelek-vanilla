import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3

import Qt.labs.folderlistmodel 2.1
import QtQuick.LocalStorage 2.0
import QtQuick.XmlListModel 2.0

import "db/db.js" as DB
import GelekBackend 1.0
import "components"

Page {
    anchors.fill: parent

    header: GelekHeader {
        id: levelSavedGamesHeader
        title: i18n.tr("Saved Games")
        flickable: savedGames
    }

    property int sectionNumber

    ListView {
        id: savedGames
        anchors.fill: parent

        header: Column {
            id: savedGameTitle
            spacing: units.gu(2)
            width: parent.width

            Label {
                text: i18n.tr("List of Saved Games")
                color: darkColor
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignBottom
                width: parent.width
                height: units.gu(10)
                font.bold: true
                textSize: Label.Large
            }

            Text {
                anchors.horizontalCenter: parent.horizontalCenter
                color: theme.palette.normal.backgroundSecondaryText

                text: savedGamesModel.count === 0
                    ? i18n.tr("No saved games found")
                    : i18n.tr("Slide for options")
            }
        }

        section {
            property: "category"
            criteria: ViewSection.FullString

            delegate: ListItemHeader {
                title: section
            }
        }

        model: sortedSavedGamesModel
        delegate: savedGamesDelegate
    }

    Component {
        id: savedGamesDelegate

        ListItem {
            property string wichGame;

            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            leadingActions: ListItemActions {
                actions: Action {
                    iconName: "delete"
                    text: i18n.tr("Delete")

                    onTriggered: {
                        GelekBackend.deleteSavedGames(filePath)
                        console.log(fileName)
                        GelekBackend.deleteSave(fileName)
                    }
                }
            }

            trailingActions: ListItemActions {
                actions: [
                    Action {
                        iconName: "share"
                        text: i18n.tr("Share")

                        onTriggered: {
                            //Open the click with Telegram, OpenStore, etc.
                            var sharePage = mainPageStack.push(
                                Qt.resolvedUrl("SharePage.qml"), {
                                    "url": filePath,
                                    "contentType": ContentType.All,
                                    "handler": ContentHandler.Share
                                });
                        }

                    },
                    Action {
                        iconName: "save"
                        text: i18n.tr("Save")

                        onTriggered: {
                            //Save with File Manger, etc.
                            var installPage = mainPageStack.push(
                                Qt.resolvedUrl("SharePage.qml"), {
                                    "url": filePath,
                                    "contentType": ContentType.All,
                                    "handler": ContentHandler.Destination
                                });
                        }
                    }
                ]
            }

            ListItemLayout {
                id: showItem
                title.text: fileBaseName

                Icon {
                    width: units.gu(3); height: width
                    source: "../assets/save.svg"
                    SlotsLayout.position: SlotsLayout.Leading
                }
            }
        }
    }

    FolderListModel {
        id: savedGamesModel
        rootFolder: savedGamesURL
        folder: savedGamesURL
        //With Terps that let you insert filename, savegames can be of any (or no) file extension
        //nameFilters: [ "*.glksave", "*.GLKSAVE" ]
        showHidden: true
        showDirs: false
        sortField: FolderListModel.Time
        onDataChanged: {
            updateModels();
            console.log("Data changed") ;
        }

        onCountChanged: updateModels()

        function updateModels() {
            console.log("Updating Models")
            categorizedSavedGamesModel.clear()
            var game = {};
            var gameName = "";

            for (var i=0; i< savedGamesModel.count; ++i) {
                if (savedGamesModel.get(i, "fileName").indexOf("gelekSaveList") == -1 && savedGamesModel.get(i, "fileName").indexOf("gelekSaveListOLD") == -1) {
                    game = DB.getGameFromSaved(savedGamesModel.get(i, "fileName"));
                    gameName = game.gameName

                    if (gameName == null || gameName == "" || gameName == "Unknown") {
                        if (game.gameIFID == null || game.gameIFID == "") {
                            gameName = i18n.tr("Unknown");
                        } else {
                            //TODO: Look up for ifiction file and get the title
                            gameName = game.gameIFID
                        }
                    }

                    categorizedSavedGamesModel.append({
                        category: gameName,
                        fileName: savedGamesModel.get(i, "fileName"),
                        fileBaseName: savedGamesModel.get(i, "fileBaseName"),
                        filePath: savedGamesModel.get(i, "filePath")
                    });
                }
            }
        }
    }

    ListModel {
        id: categorizedSavedGamesModel
    }

    SortFilterModel {
        id: sortedSavedGamesModel
        model: categorizedSavedGamesModel

        sort {
            property: "category"
            order: Qt.DescendingOrder
        }
    }
}

import QtQuick 2.9
import Lomiri.Components 1.3
import "components"

Page {
    id: aboutPage
    title: "Settings"

    signal close

    //Margins
    property int marginColumn: units.gu(1)

    header: BaseHeader {
        id: pageHeaderSettings
        title: i18n.tr("Settings")
        flickable: settingsFlickable
    }

    anchors.fill: parent

    Flickable {
        id: settingsFlickable
        anchors.fill: parent

        contentHeight: mainColumnSettings.height + units.gu(8)

        Column {
            id: mainColumnSettings
            width: parent.width  - marginColumn * 4
            spacing: units.gu(4)

            anchors {
                top: parent.top
                topMargin: units.gu(4)
                horizontalCenter: parent.horizontalCenter
            }

            Column {
                width: parent.width
                spacing: units.gu(3)

                Label {
                    text: i18n.tr("General Settings")
                    font.bold: true
                    color: darkColor
                }

                Label {
                    width: parent.width - units.gu(2)
                    text: i18n.tr("Theme")
                }

                ListItem {
                    id: themeTF
                    width: parent.width
                    height: cTheme.height > 0
                        ? cTheme.height
                        : units.gu(7)
                    divider.visible: false
                    highlightColor: mainView.highlightColor

                    property string themeName

                    ChooserTheme {
                        id: cTheme
                        width: parent.width

                        onSelectedThemeChanged: themeTF.themeName = selectedTheme;
                    }
                }
            }

            Label {
                text: i18n.tr("Convergence Settings")
                font.bold: true
                color: darkColor
            }

            Column {
                width: parent.width
                spacing: units.gu(6)

                //TODO: When cm is false, and switch off pttr or smttr (both true previously)
                // cm comes true
                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width  - cm.width - units.gu(2)
                        text: i18n.tr("Convergence")
                    }

                    Switch {
                        id: cm
                        checked: settings.convergenceMode

                        onClicked: settings.convergenceMode = !settings.convergenceMode
                    }
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width  - ptr.width - units.gu(2)
                        text: i18n.tr("Pictures to the right")
                        color: settings.convergenceMode ? theme.palette.normal.backgroundText : theme.palette.disabled.backgroundText
                    }

                    Switch {
                        id: ptr
                        opacity: settings.convergenceMode ? 1 : 0.5
                        checked: settings.pictureToTheRight

                        onClicked: {
                            settings.pictureToTheRight = !settings.pictureToTheRight
                            //If no pictures to the right  and no menu, there's no convergence option
                            settings.convergenceMode = checked || settings.showMenuToTheRight;
                            cm.checked = settings.convergenceMode
                        }
                    }
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - smr.width - units.gu(2)
                        text: i18n.tr("Show menu to the right")
                        color: settings.convergenceMode ? theme.palette.normal.backgroundText : theme.palette.disabled.backgroundText
                    }

                    Switch {
                        id: smr
                        opacity: settings.convergenceMode ? 1 : 0.5
                        checked: settings.showMenuToTheRight

                        onClicked: {
                            settings.showMenuToTheRight = !settings.showMenuToTheRight
                            //If no pictures to the right  and no menu, there's no convergence option
                            settings.convergenceMode = settings.pictureToTheRight || checked
                            cm.checked = settings.convergenceMode
                        }
                    }
                }
            }

            Label {
                text: i18n.tr("Game Settings")
                font.bold: true
                color: darkColor
            }

            Column {
                width: parent.width
                spacing: units.gu(6)


                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width  - sip.width - units.gu(2)
                        text: i18n.tr("Show ingame pictures")
                    }

                    Switch {
                        id: sip
                        checked: settings.showPicturesInGame

                        onClicked: settings.showPicturesInGame = !settings.showPicturesInGame
                    }
                }

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width  - dff.width - units.gu(2)
                        text: i18n.tr("Don't force focus on input")
                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                    }

                    Switch {
                        id: dff
                        checked: settings.dontForceFocus

                        onClicked: settings.dontForceFocus = !settings.dontForceFocus
                    }
                }
            }

            Label {
                text: i18n.tr("Development Settings")
                font.bold: true
                color: darkColor
            }

            Column {
                width: parent.width
                spacing: units.gu(6)

                Row {
                    width: parent.width
                    spacing: units.gu(2)

                    Label {
                        width: parent.width - edl.width - units.gu(2)
                        text: i18n.tr("Enable debug logs")
                    }

                    Switch {
                        id: edl
                        checked: settings.debugging

                        onClicked: settings.debugging = !settings.debugging
                    }
                }
            }
        }
    }

    Component.onDestruction: close()
}


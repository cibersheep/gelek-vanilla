/*
 * Copyright (C) 2016 Stefano Verzegnassi
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License 3 as published by
 * the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see http://www.gnu.org/licenses/.
 */

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Content 1.3
import "components"

Page {
    id: picker
    property var activeTransfer: undefined

    property string url
    property alias handler: contentPeerPicker.handler
    property alias contentType: contentPeerPicker.contentType

    signal close()

    onClose: pageStack.pop()

    header: BaseHeader {
        title: contentPeerPicker.handler == ContentHandler.Share
            ? i18n.tr("Share with")
            : i18n.tr("Save with")
    }

    ContentPeerPicker {
        id: contentPeerPicker
        anchors { fill: parent; topMargin: picker.header.height }
        visible: parent.visible
        showTitle: false

        onPeerSelected: {
            picker.activeTransfer = peer.request();
            stateChangeConnection.target = picker.activeTransfer;
        }

        onCancelPressed: {
            close();
        }
    }

    Connections {
        id: stateChangeConnection
        target: null

        onStateChanged: {
            switch (picker.activeTransfer.state) {
                case ContentTransfer.Aborted:
                    console.log("Aborted")
                    break;
                case ContentTransfer.Collected:
                    console.log("Collected")
                    //Don't finalize() on Collented if Share or Destination
                    //activeTransfer.finalize();
                    close()
                    break;
                case ContentTransfer.Finalized:
                    console.log("Finalized")
                    break;
                case ContentTransfer.InProgress:
                    console.log("In progress", url);
                    resultComponent.url = "file://" + url
                    picker.activeTransfer.items = [ resultComponent ];
                    picker.activeTransfer.state = ContentTransfer.Charged;
                    break;
                case ContentTransfer.Charged:
                    console.log("Charged")
                    break;
                default:
                  console.log("Other state?",activeTransfer.state)
            }
        }
    }

    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: picker.activeTransfer
    }

    ContentItem {
        id: resultComponent
    }
}

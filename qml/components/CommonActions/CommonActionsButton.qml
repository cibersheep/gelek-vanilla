import QtQuick 2.9
import Lomiri.Components 1.3

Button {
    property string txtShown
    property string txtCommand

    width: parent.width
    color: lightColor
    text: txtShown

    onClicked: {
        command.text = txtCommand
        if (bottomEdgeVisible) bottomEdge.collapse()
    }
}

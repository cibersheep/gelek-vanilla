import QtQuick 2.9
import Lomiri.Components 1.3

Button {
    property string txtShown
    property string txtCommand
    property string iconShown
    property string iconsourceShown
    property alias iconRotation: icon.rotation

    width: units.gu(4)
    color: lightColor

    Icon {
        id: icon
        width: units.gu(2)
        anchors.centerIn: parent
        color: "#fff" //lighterColor
        name: iconShown
    }

    Component.onCompleted: if (iconsourceShown !== "") icon.source = iconsourceShown

    onClicked: {
        command.text = txtCommand
        instruction.clicked()
        if (bottomEdgeVisible) bottomEdge.collapse()
        command.focus = true
    }
}

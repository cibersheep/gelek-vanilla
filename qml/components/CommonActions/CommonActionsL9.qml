import QtQuick 2.9
import Lomiri.Components 1.3

Grid {
    id: mainGrid
    spacing: units.gu(2)
    width: parent.width - units.gu(4)

    anchors {
        margins: units.gu(2)
        horizontalCenter: parent.horizontalCenter
    }

    Column {
        width: (parent.width / 3) - units.gu(2)
        spacing: units.gu(2)

        CommonActionsButton {
            txtShown: "Examine"
            txtCommand: "EXAMINE "
        }

        CommonActionsButton {
            txtShown: "Open"
            txtCommand: "OPEN "
        }

        CommonActionsButton {
            txtShown: "Close"
            txtCommand: "CLOSE "
        }

        CommonActionsButton {
            txtShown: "Pull"
            txtCommand: "PULL "
        }

        CommonActionsButton {
            txtShown: "Push"
            txtCommand: "PUSH "
        }
     }

    Column {
        width: (parent.width / 3) - units.gu(2)

        spacing: units.gu(2)

        CommonActionsCommand  {
            txtShown: "Inventory"
            txtCommand: "INVENTORY"
        }

        CommonActionsButton {
            txtShown: "Get"
            txtCommand: "GET "
        }

        CommonActionsButton {
            txtShown: "Drop"
            txtCommand: "DROP "
        }

        CommonActionsButton {
            txtShown: "Wear"
            txtCommand: "WEAR "
        }

        CommonActionsCommand  {
            txtShown: "Score"
            txtCommand: "SCORE"
        }
    }

    Column {
        width: (parent.width / 3)
        spacing: units.gu(2)

        CommonActionsCommand  {
            txtShown: "Save"
            txtCommand: "SAVE"
        }

        CommonActionsCommand  {
            txtShown: "Restore"
            txtCommand: "RESTORE"
        }

        WindRose {}

    }
}

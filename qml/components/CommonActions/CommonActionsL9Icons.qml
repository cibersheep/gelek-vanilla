import QtQuick 2.9
import Lomiri.Components 1.3

Flow {
    id: mainGrid
    width: parent.width
    spacing: units.gu(1.5)

    CommonActionsButtonIcon {
        iconShown: "search"
        txtCommand: "EXAMINE "
    }

    CommonActionsButtonIcon {
        iconsourceShown: "../../../assets/unlock.svg"
        txtCommand: "OPEN "
    }

    CommonActionsButtonIcon {
        iconShown: "lock"
        txtCommand: "CLOSE "
    }

    CommonActionsButtonIcon {
        iconsourceShown: "../../../assets/pull.svg"
        txtCommand: "PULL "
    }

    CommonActionsButtonIcon {
        iconsourceShown: "../../../assets/push.svg"
        txtCommand: "PUSH "
    }

    CommonActionsCommandIcon  {
        iconShown: "edit-paste"
        txtCommand: "INVENTORY"
    }

    CommonActionsButtonIcon {
        iconShown: "up"
        txtCommand: "GET "
    }
    
    CommonActionsButtonIcon {
        iconShown: "down"
        txtCommand: "DROP "
    }
    
    CommonActionsButtonIcon {
        iconsourceShown: "../../../assets/wear.svg"
        txtCommand: "WEAR "
    }
    
    CommonActionsCommandIcon  {
        iconShown: "starred"
        txtCommand: "SCORE"
    }

    CommonActionsCommandIcon  {
        iconShown: "save"
        txtCommand: "SAVE"
    }

    CommonActionsCommandIcon  {
        iconsourceShown: "../../../assets/restore.svg"
        iconRotation: 180
        txtCommand: "RESTORE"
    }
}

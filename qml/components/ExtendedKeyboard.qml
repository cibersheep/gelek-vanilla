import QtQuick 2.9
import Lomiri.Components 1.3

Row {
    spacing: units.gu(2)

    anchors.margins: units.gu(2)

    Button {
        width: units.gu(6)
        height: units.gu(4)
        color: darkColor
        opacity: enabled ? 1 : .4
        //Make sure the touch area is big enough
        sensingMargins.all: units.gu(2)

        Icon {
            width: units.gu(2)
            anchors.centerIn: parent
            color: lighterColor
            name: "keyboard-tab"
        }

        onClicked: extededKeyboard.processChar("tab")
    }

    Button {
        width: units.gu(4)
        height: width
        color: darkColor
        opacity: enabled ? 1 : .4
        //Make sure the touch area is big enough
        sensingMargins.all: units.gu(2)

        Icon {
            width: units.gu(2)
            anchors.centerIn: parent
            color: lighterColor
            name: "toolkit_arrow-left"
        }

        onClicked: extededKeyboard.processChar("left")
    }

    Button {
        width: units.gu(4)
        height: width
        color: darkColor
        opacity: enabled ? 1 : .4
        //Make sure the touch area is big enough
        sensingMargins.all: units.gu(2)

        Icon {
            width: units.gu(2)
            anchors.centerIn: parent
            color: lighterColor
            name: "toolkit_arrow-up"
        }

        onClicked: extededKeyboard.processChar("up")
    }

    Button {
        width: units.gu(4)
        height: width
        color: darkColor
        opacity: enabled ? 1 : .4
        //Make sure the touch area is big enough
        sensingMargins.all: units.gu(2)

        Icon {
            width: units.gu(2)
            anchors.centerIn: parent
            color: lighterColor
            name: "toolkit_arrow-down"
        }

        onClicked: extededKeyboard.processChar("down")
    }

    Button {
        width: units.gu(4)
        height: width
        color: darkColor
        opacity: enabled ? 1 : .4
        //Make sure the touch area is big enough
        sensingMargins.all: units.gu(2)

        Icon {
            width: units.gu(2)
            anchors.centerIn: parent
            color: lighterColor
            name: "toolkit_arrow-right"
        }

        onClicked: extededKeyboard.processChar("right")
    }

    function processChar(character) {
        JsParser.commandToBuffer(character);
    }
}

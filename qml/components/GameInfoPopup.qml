import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import Lomiri.DownloadManager 1.2
import QtQuick.XmlListModel 2.0

import "../js/gameFormatHelper.js" as Format
import GelekBackend 1.0

Dialog {
    id: infoDialog
    title: i18n.tr("Game Info")

    property string gameURLtoGetInfoFrom
    property string fileType

    Component.onCompleted: {
        GelekBackend.babel("-ifid", gameURLtoGetInfoFrom)
        GelekBackend.babel("-format", gameURLtoGetInfoFrom)

        //Leave this for later. We don't use it yet
        //GelekBackend.babel("-identify", gameURLtoGetInfoFrom)

        //Check for known erroneous IFID
        gameIFID = Format.exceptionalIFID(gameIFID)

        ifictionFile = ""

        var gameRoute = gameURLtoGetInfoFrom.split(/\//g)
        var folderURL = gameURLtoGetInfoFrom.slice(0, gameURLtoGetInfoFrom.indexOf(gameRoute[gameRoute.length -1]))
        ifictionFile = "file://" + folderURL + gameIFID + ".ifiction"
        gameCover = "file://" + folderURL + gameIFID
        console.log("Debug: " + ifictionFile + "\n" + gameCover)

        image.source = gameCover
        xmlModel.reload()
    }

    LomiriShape {
        id: iconTop
        height: iconTop.width
        anchors.horizontalCenter: parent.horizontalCenter
        aspect: LomiriShape.Flat
        sourceFillMode: LomiriShape.PreserveAspectFit

        source: Image {
            id: image

            //Set the source image for cover onCompleted
            //https://gitlab.com/cibersheep/gelek-vanilla/issues/1
            onStatusChanged: {
                if (image.status == Image.Error) {
                    image.source = "qrc:/assets/gelek.svg"
                }
            }
        }
    }

    ListView {
        id: ifictionListView
        width: parent.width
        height: contentItem.childrenRect.height
        interactive: false
        model: xmlModel
        delegate: xmlDelegate
    }

    Text {
        id: ifidText
        text: "IFID: " + (gameIFID || "")
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        color: theme.palette.normal.overlayText
    }

    Text {
        id: formatText
        text: i18n.tr("Format: %1").arg(gameFormat || "")
        color: theme.palette.normal.overlayText
    }

    Label {
        id: alertText
        width: parent.width
        horizontalAlignment: Text.AlignCenter
        visible: xmlModel.count == 0
        text: i18n.tr("Ifiction file doesn't exist!")
        color: theme.palette.normal.overlayText
    }

    Rectangle {
        id: downloadingBar
        visible: (downloadIfile.errorMessage == "" && downloadCover.errorMessage == "") && (downloadCover.downloadInProgress || downloadIfile.downloadInProgress)
        height: downloadFileButton.height
        color: theme.palette.normal.background

        ProgressBar {
            anchors.verticalCenter: parent.verticalCenter
            visible: parent.visible
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width - units.gu(2)
            value: (width / 300) * ((downloadCover.progress + downloadIfile.progress))
        }
    }

    Button {
        id: downloadFileButton
        text: xmlModel.count == 0
            ? i18n.tr("Download")
            : i18n.tr("Re-download")

        visible: !downloadingBar.visible
        
        enabled: downloadIfile.errorMessage == "" && downloadCover.errorMessage == ""

        onClicked: {
            //If reload, delete files first or the new are not downloaded
            if (ifictionFile !== "") {
                GelekBackend.deleteSavedGames(ifictionFile)
                GelekBackend.deleteSavedGames(gameCover)
            }

            downloadIfile.download(Qt.resolvedUrl("http://ifdb.org/viewgame?ifiction&ifid=" + gameIFID))
            downloadCover.download(Qt.resolvedUrl("http://ifdb.org/viewgame?coverart&ifid=" + gameIFID))
        }
    }

    Button {
        //If error cannot cancel the download
        text: (downloadIfile.errorMessage == "" && downloadCover.errorMessage == "") && (downloadCover.downloadInProgress || downloadIfile.downloadInProgress)
            ? i18n.tr("Cancel")
            : i18n.tr("Close")

        onClicked: {
            if ((downloadIfile.errorMessage == "" && downloadCover.errorMessage == "") && (downloadCover.downloading || downloadIfile.downloading)) {
                console.log("Cancel download")
                downloadIfile.cancel();
                downloadCover.cancel();
            } else {
                console.log("Close")
                PopupUtils.close(infoDialog);
            }
        }
    }

    SingleDownload {
        id: downloadIfile
        autoStart: true

        onFinished: {
            console.log("Download path: " + path)
            //Saved ifictionFile route is returned by storeGameFile()
            ifictionFile = "file://" + GelekBackend.storeGameFile(path, gameIFID + ".ifiction")
            xmlModel.reload()
        }

        onErrorMessageChanged: {
            console.log("Error downloading .ifiction file:",errorMessage, "downloadIfile.downloadInProgress",downloadIfile.downloadInProgress, downloadIfile.progress, downloadCover.progress);
            downloadIfile.cancel();
            if (errorMessage == "HttpError: 404 - Not Found") {
                alertText.text = i18n.tr("Ifiction can't be found online")
            }
        }
    }

    SingleDownload {
        id: downloadCover
        autoStart: true

        onFinished: {
            console.log("Download cover path: " + path)
            //Let's store the downloaded iFiction file in the same folder as the game
            //Saved cover route is returned by storeGameFile()
            gameCover = "file://" + GelekBackend.storeGameFile(path, gameIFID)
            image.source = gameCover
        }

        onErrorMessageChanged: {
            console.log("Error downloading cover:",errorMessage)
            downloadCover.cancel()
        }
    }

    XmlListModel {
        id: xmlModel
        source: ifictionFile
        query: "/ifindex/story"
        namespaceDeclarations: "declare default element namespace 'http://babel.ifarchive.org/protocol/iFiction/';"
        XmlRole { name: "title"; query: "bibliographic/title/string()" }
        XmlRole { name: "author"; query: "bibliographic/author/string()" }
        XmlRole { name: "language"; query: "bibliographic/language/string()" }
        XmlRole { name: "firstpublished"; query: "bibliographic/firstpublished/string()" }
        XmlRole { name: "cover"; query: "ifdb/coverart/url/string()" }
    }

    Component {
        id: xmlDelegate

        Column {
            width: parent.width
            spacing: units.gu(2)

            Text {
                id: titleText
                text: title + ", " + new Date(firstpublished).toLocaleDateString(Qt.locale(), Locale.ShortFormat) + " (" + language + ")"
                font.pointSize: units.gu(1.4)
                font.bold: true
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: theme.palette.normal.overlayText
            }

            Text {
                id: authorsText
                text: i18n.tr("Authors: %1").arg(author)
                width: parent.width
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                color: theme.palette.normal.overlayText
            }
        }
    }
}

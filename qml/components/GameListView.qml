import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

Component {
    ListItem {
        width: parent.width
        anchors.horizontalCenter: parent.horizontalCenter
        divider.visible: false
        clip: true
        highlightColor: selectionColor

        leadingActions: ListItemActions {
            actions: Action {
                iconName: "delete"
                text: i18n.tr("Delete")

                onTriggered: {
                    //Surt de la cerca si n'hi ha
                    pageHeader.closeHeader()
                    mainView.deleteGame(filePath)

                }
            }
        }

        trailingActions: ListItemActions {
            actions: [
                Action {
                    iconName: "swap"
                    text: i18n.tr("Swap interpreter")

                    onTriggered: {
                        PopupUtils.open(
                            Qt.resolvedUrl("TerpSwapPopup.qml"),
                            null,
                            {"gameUrl": filePath}
                        );
                    }
                },
                Action {
                    iconName: "info"
                    text: i18n.tr("Info")

                    onTriggered: {
                        PopupUtils.open(
                            Qt.resolvedUrl("GameInfoPopup.qml"),
                            null,
                            {"gameURLtoGetInfoFrom": filePath}
                        );
                    }
                }
            ]
        }

        ListItemLayout {
            id:layout
            title.text: fileName
            width: parent.width - marginColumn * 9
            anchors.horizontalCenter: parent.horizontalCenter

            Icon {
                source: "../../assets/game.svg"
                SlotsLayout.position: SlotsLayout.Leading
                width: units.gu(3)
            }
        }

        onClicked: {
            //Surt de la cerca si n'hi ha
            pageHeader.closeHeader()
            mainView.runGame(filePath)
        }
    }
}

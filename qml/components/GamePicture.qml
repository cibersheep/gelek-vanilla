import QtQuick 2.9
import Lomiri.Components 1.3

LomiriShape {
    property alias img: img
    property real uShapeRatio: height/width
    property real imgRatio: clipWidth == -1
        ? viewboxh/canvasHeight
        : viewboxw/clipWidth * .9

    visible: settings.showPicturesInGame && canvasHeight > 0
    width: isLandscape
        ? settings.pictureToTheRight
            ? Math.min(Math.max(canvasWidth, (parent.width  - units.gu(6)) * .45), (parent.height -units.gu(10)) / 3  * (canvasWidth/canvasHeight) + 6)
            : Math.min(Math.max(canvasWidth, (parent.width  - units.gu(6)) * .55), (parent.height -units.gu(10)) / 3  * (canvasWidth/canvasHeight) + 6)
        : Math.min(Math.max(viewboxw, parent.width - units.gu(2)), (parent.height -units.gu(10))/ 3 * (viewboxw/viewboxh) + 6)
    height: width * (canvasHeight/canvasWidth) + 6
    backgroundColor: Theme.palette.normal.background
    aspect: LomiriShape.Flat

    sourceVerticalAlignment: LomiriShape.AlignVCenter
    sourceHorizontalAlignment: LomiriShape.AlignHCenter
    sourceScale: Qt.vector2d( imgRatio, imgRatio)

    anchors {
        horizontalCenter: gameFlickable.horizontalCenter
        horizontalCenterOffset: settings.pictureToTheRight && isLandscape
            ? gameFlickable.width
            : 0
        top: gameHeader.visible
            ? gameHeader.bottom
            : parent.top
        topMargin: gridLinesRec.visible
            ? gridLinesRec.height
            : 0
    }

    source: Canvas {
        id: img

        property var imgToLoad
        property int imgX //unused. Should be draImage coordinates
        property int imgY

        width: canvasWidth
        height: canvasHeight

        onImageLoaded: {
            var ctx = img.getContext("2d");
            ctx.drawImage(imgToLoad, 0, 0, width, height)
            img.requestPaint()
        }
    }
}

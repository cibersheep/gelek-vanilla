import QtQuick 2.9
import Lomiri.Components 1.3

BaseHeader {
    signal keepCheckingFocus(bool isChecking)

    trailingActionBar {
        numberOfSlots: 2

        actions: [
            Action {
                id: actionInfo
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")

                onTriggered: {
                    keepCheckingFocus(false)
                    Qt.inputMethod.hide();
                    var about = mainPageStack.push(Qt.resolvedUrl("../About.qml"));
                    about.close.connect(function() {
                        keepCheckingFocus(true);
                    });
                }
            },

            Action {
                id: actionSettings
                iconName: "settings"
                shortcut: "Ctrl+s"
                text: i18n.tr("Settings")

                onTriggered: {
                    keepCheckingFocus(false)
                    Qt.inputMethod.hide();
                    var settings = mainPageStack.push(Qt.resolvedUrl("../SettingsPage.qml"));
                    settings.close.connect(function() {
                        keepCheckingFocus(true);
                    });
                }
            }
        ]
    }
}

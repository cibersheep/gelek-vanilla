import QtQuick 2.9
import Lomiri.Components 1.3

ListItemLayout {
    title.text: root.title.text
    title.color: lightColor
    ProgressionSlot {}
}

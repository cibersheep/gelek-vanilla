import QtQuick 2.9
import Lomiri.Components 1.3

Item {
    width: parent.width
    height: inputArea.height

    MouseArea {
        //Make sure we capture taps on the inputArea area
        anchors {
            bottom: inputArea.bottom
            left: parent.left
            right: parent.right
        }

        height: inputArea.height + units.gu(1)

        onReleased: command.focus = true;
    }

    Row {
        id: inputArea
        spacing: units.gu(1)
        width: gameFlickable.width - instruction.width

        anchors {
            left: parent.left
            right: isLandscape && (gameImage.visible || commonsActionsColumnL9.visible)
                ? undefined
                : parent.right
            bottom: parent.bottom
            margins: units.gu(2)
        }

        TextField {
            id: command
            width: parent.width - instruction.width

            //Avoid Language aids to the enable return buttons properly
            //See: https://gitlab.com/ubports-linphone/linphone-simple/issues/16
            inputMethodHints: Qt.ImhNoPredictiveText
            //If this property is defined, onAccepted() will make the rect flick and everything move

            onTextChanged: if (text.length === 1 && response.input[0].type === "char") instruction.clicked()

            onAccepted: {
                command.focus=false;
                //command.focus=true; //Let the onFocusChanged take care of this

                if (!showNextScroll) {
                    commandToBuffer(command.text);
                    command.text = "";
                } else {
                    flickText(gameFlickable.scrollLeft)
                }
            }

            onFocusChanged: {
                if (!command.focus && checkFocus && !bottomEdgeVisible) {
                    command.focus = true
                }
            }
        }

        Button {
            id: instruction
            width: units.gu(4)
            height: width
            enabled: command.text !== "" || showNextScroll
            color: darkColor
            opacity: enabled ? 1 : .4
            //Make sure the touch area is big enough
            sensingMargins.all: units.gu(2)

            Icon {
                width: units.gu(2)
                anchors.centerIn: parent
                color: instruction.enabled
                    ? lighterColor
                    : Theme.palette.disabled.foregroundText
                name: showNextScroll ? "add" : "keyboard-enter"
            }

            onClicked: command.accepted()
        }
    }
}

/*
 */

import QtQuick 2.9
import Lomiri.Components 1.3

BaseHeader {
    signal searchText(string typedText)

    property bool isSearching: false
    property alias title: mainTitle.text

    contents: Row {
        anchors.centerIn: parent
        width: parent.width
        spacing: units.gu(1.5)

        Label {
            id: mainTitle
            visible: !isSearching
            textSize: Label.Large
            color: lighterColor
        }

        TextField {
            id: searchField
            visible: isSearching
            width: parent.width

            // Disable predictive text
            inputMethodHints: Qt.ImhNoPredictiveText

            primaryItem: Icon {
                width: units.gu(2); height: width
                name: "find"
            }

            placeholderText: i18n.tr("Search name…")

            onTextChanged: searchText(text);
        }
    }

    leadingActionBar.actions: Action {
        iconName: "close"
        visible: isSearching
        text: i18n.tr("Close")

        onTriggered: closeHeader();
    }

    trailingActionBar {
        actions: [
            Action {
                id: actionInfo
                iconName: "info"
                shortcut: "Ctrl+i"
                text: i18n.tr("Information")

                onTriggered: {
                    mainPageStack.push(Qt.resolvedUrl("../About.qml"));
                }
            },
            Action {
                id: actionSettings
                iconName: "settings"
                shortcut: "Ctrl+s"
                text: i18n.tr("Settings")

                onTriggered: {
                    Qt.inputMethod.hide();
                    mainPageStack.push(Qt.resolvedUrl("../SettingsPage.qml"));
                }
            },
            Action {
                iconName: "search"
                text: i18n.tr("Search")
                visible: !isSearching

                onTriggered: {
                    isSearching = true;
                    searchField.focus = true;
                }
            }
        ]
    }

    function closeHeader() {
        searchField.text = "";
        isSearching = false;
        searchField.focus = false;
    }
}

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import Lomiri.DownloadManager 1.2
import QtQuick.XmlListModel 2.0

import "../js/gameFormatHelper.js" as Format
import GelekBackend 1.0

Dialog {
    id: multipleImportsDialog
    title: i18n.tr("Files imported")

    property var filesModel
    signal close()
    signal chosen(string gameChosen)

    onFilesModelChanged: {
        console.log("List MODEL changed")
        importedFilesModel.append(filesModel)
    }

    ListModel {
        id: importedFilesModel
    }

    Text {
        id: ifidText
        text: i18n.tr("Choose the game you want to play")
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        color: theme.palette.normal.overlayText
    }

    ListView {
        id: filesListView
        width: parent.width
        height: contentItem.childrenRect.height
        interactive: false
        delegate: filesListDelegate
        model: importedFilesModel
    }

    Button {
        //If error cannot cancel the download
        text: i18n.tr("Close")

        onClicked: {
            console.log("Close")
            multipleImportsDialog.close()
        }
    }

    Component {
        id: filesListDelegate

        ListItem {
            height: filesDelegateLayout.height
            divider.visible: false
            highlightColor: mainView.selectionColor

            ListItemLayout {
                id: filesDelegateLayout
                title.text: gameName
                subtitle.text: url.replace(mainView.baseRootURL, "[...]/")
                ProgressionSlot {}
            }

            onClicked: {
                multipleImportsDialog.close()
                multipleImportsDialog.chosen(url)
            }
        }
    }
}

import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3

import GelekBackend 1.0

Dialog {
    id: dialogueRestore

    property bool gameRestoredOrCancelled: false

    title: i18n.tr("Restore Game")
    text: savedGamesModel.count === 0 ? i18n.tr("There's no saved games yet") : i18n.tr("Choose a saved game to restore")

    ScrollView {
        height: savedGamesModel.count === 0 ? units.gu(1) : units.gu(25)

        ListView {
            id: savedGameList
            width: parent.width
            clip: true
            snapMode: ListView.SnapToItem

            model: savedGamesModel
            delegate: savedGamesDelegate
        }
    }

    Component {
        id: savedGamesDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: fileName
                width: parent.width

                Icon {
                    source: "../../assets/save.svg"
                    SlotsLayout.position: SlotsLayout.Leading
                    width: units.gu(3)
                }
            }

            onClicked: {
                GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "' + mainView.savedGamesURL.replace("file://","") + fileName + '" }')
                gameRestoredOrCancelled = true
                PopupUtils.close(dialogueRestore)
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "" }')
            gameRestoredOrCancelled = true
            PopupUtils.close(dialogueRestore)
        }
    }

    SavedGamesModel {
        id: savedGamesModel
    }

    Component.onDestruction: {
        //Prevent remGlk terp stop when Dialog is dismissed by Esc key stroke
        if (!gameRestoredOrCancelled) {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt"}')
        }
    }
}

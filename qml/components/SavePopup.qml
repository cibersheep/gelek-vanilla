import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0

import GelekBackend 1.0
import "../db/db.js" as DB

Dialog {
    id: dialogue

    property bool nameExists: false
    property bool gameSavedOrCancelled: false

    title: i18n.tr("Save Game")
    text: i18n.tr("Enter a name to save the game")

    Label {
        id: alertText
        horizontalAlignment: Text.AlignCenter
        visible: nameExists
        text: i18n.tr("File name already exists!")
        color: theme.palette.normal.negative
    }

    TextField {
        id: saveGameName
        onAccepted: saveButton.clicked()
        inputMethodHints: Qt.ImhNoPredictiveText
    }

    Button {
        id: saveButton
        text: i18n.tr("Save")
        color: theme.palette.normal.positive
        enabled: saveGameName.text !=="" && saveGameName.text !== " "

        onClicked: {
            nameExists = false;

            for (var i=0; i < savedGamesModel.count; i++){
                if (savedGamesModel.get(i, "fileBaseName") === saveGameName.text) {
                    console.log("Debug: File name exists")
                    nameExists = true
                    alertFade.start()
                    break
                }
            }

            if (!nameExists) {
                if (settings.debugging) console.log("Debug: Game name to save: " + saveGameName.text)

                GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "' + mainView.savedGamesURL.replace("file://","") + saveGameName.text + '.glksave" }')
                DB.storeSavedGame(Qt.formatDateTime(new Date(), "dd-MM-yyyy, HH:mm:ss"), mainView.getNameFromUrl(game), gameIFID, saveGameName.text + '.glksave')
                gameSavedOrCancelled = true
                PopupUtils.close(dialogue)
            }
        }
    }

    Button {
        text: i18n.tr("Cancel")

        onClicked: {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt", "value": "" }')
            gameSavedOrCancelled = true
            PopupUtils.close(dialogue)
        }
    }

    SavedGamesModel {
        id: savedGamesModel
    }

    Timer {
        id: alertFade
        interval: 3000

        onTriggered: nameExists = false
    }

    Component.onDestruction: {
        //Prevent remGlk terp stop when Dialog is dismissed by Esc key stroke
        if (!gameSavedOrCancelled) {
            GelekBackend.sendCommand('{ "type": "specialresponse", "gen": ' + response.gen + ', "response": "fileref_prompt"}')
        }
    }
}

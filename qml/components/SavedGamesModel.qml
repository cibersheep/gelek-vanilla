import QtQuick 2.9
import Lomiri.Components 1.3

import Qt.labs.folderlistmodel 2.1

FolderListModel {
    rootFolder: mainView.savedGamesURL
    folder: mainView.savedGamesURL
    nameFilters: [ "*.geleksave", "*.GELEKSAVE", "*.glksave", "*.GLKSAVE" ]
    showHidden: true
    showDirs: false
    //sortField: Name
    sortField: FolderListModel.Time
}

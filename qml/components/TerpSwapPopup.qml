import QtQuick 2.9
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.LocalStorage 2.0

import "../db/db.js" as DB
import GelekBackend 1.0

Dialog {
    id: terpChooserDialog
    title: i18n.tr("Choose an Interpreter")

    property string gameUrl
    property string fileType
    property var saveGameData

    property var allTerps: {
        "advsys":        { "name": "AdvSys", "index": 0, "terp": "advsys" },
        "adrift":        { "name": "Adrift", "index": 1, "terp": "adrift" },
        "agt":           { "name": "Agility", "index": 2, "terp": "agt" },
        "blorbed glulx": { "name": "Glulxe", "index": 3, "terp": "blorbed glulx" },
        "glulx":         { "name": "Glulxe", "index": 3, "terp": "glulx" },
        "blorbed zcode": { "name": "Bocfel", "index": 4, "terp": "blorbed zcode" },
        "zcode":         { "name": "Bocfel", "index": 4, "terp": "zcode" },
        "hugo":          { "name": "Hugo", "index": 5, "terp": "hugo" },
        "level9":        { "name": "Level 9", "index": 6, "terp": "level9" },
        "magscrolls":    { "name": "Magnetic Scrolls", "index": 7 , "terp": "magscrolls"},
        "tads3":         { "name": "TADS", "index": 8, "terp": "tads3" },
        "tads2":         { "name": "TADS", "index": 8, "terp": "tads2" },
        "executable":    { "name": "Other", "index": 9, "terp": "executable" }
    }

    Component.onCompleted: {
        saveGameData = DB.getGameByUrl(gameUrl)

        if (!saveGameData) {
            gameFormat = saveGameData.format
            saveGameData = mainView.prepareGameNamesAndOpen(gameUrl)
        }

        gameFormat = saveGameData.format
        console.log("----- gameFormat", gameFormat)

        terpsListView.currentIndex = allTerps[gameFormat.replace(" (non-authoritative)","")].index
    }

    ListModel {
        id: terpsModel

        Component.onCompleted: {
            terpsModel.append([
                { interpreter: "AdvSys", format: "advsys" },
                { interpreter: "Adrift", format: "adrift" },
                { interpreter: "Agility", format: "agt" },
                { interpreter: "Glulxe", format: "glulx" }, //Missing blorbed versions when updating. Could be a problem in the future
                { interpreter: "Bocfel", format: "zcode" },
                { interpreter: "Hugo", format: "hugo" },
                { interpreter: "Level 9", format: "level9" },
                { interpreter: "Magnetic Scrolls", format: "mag" },
                { interpreter: "TADS", format: "tads2" },
                { interpreter: "Other", format: "executable" }
            ])
        }
    }

    Label {
        width: parent.width
        text: i18n.tr("Choose the default interpreter for this game")
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        color: theme.palette.normal.overlayText
    }

    ComboButton {
        id: combo
        text: allTerps[gameFormat.replace(" (non-authoritative)","")].name
        expanded: true
        expandedHeight: units.gu(45)
        onClicked: expanded = !expanded

        LomiriListView {
            id: terpsListView
            width: parent.width
            height: combo.comboListHeight
            highlightRangeMode: ListView.ApplyRange
            model: terpsModel
            delegate: terpListDelegate
        }
    }

    Button {
        text: i18n.tr("Done")

        onClicked: PopupUtils.close(terpChooserDialog);
    }

    Component {
        id: terpListDelegate

        ListItem {
            width: parent.width
            divider.visible: false
            clip: true
            highlightColor: selectionColor

            ListItemLayout {
                title.text: interpreter
            }

            onClicked: {
                console.log("Change to",String(format))
                DB.updateFormat(saveGameData.id, format)
                terpsListView.currentIndex = allTerps[format].index
                combo.text = allTerps[format].name
                combo.expanded = false
            }
        }
    }
}

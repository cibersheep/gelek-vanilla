/*
 * uNav http://launchpad.net/unav
 * Copyright (C) 2015 JkB https://launchpad.net/~joergberroth
 * Copyright (C) 2015 Marcos Alvarez Costales https://launchpad.net/~costales
 *
 * uNav is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * uNav is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
// Thanks http://askubuntu.com/questions/352157/how-to-use-a-sqlite-database-from-qml


/*
 * // Functions for saved games
 * storeSavedGame(revisionDate, gameName, gameID, savedGameFile)
 * getGameFromSaved(savedGameName)
 * function deleteSavedGameFromList(savedGameName)
 * deleteSavedGameFromList(savedGameName)
 *
 * //Functions for game files
 * storeGameInfo(revisionDate, gameName, gameID, format, url)

 * Find a game by it name or ifid: getGameInfo(gameName, gameID)

 * deleteGameInfoByName(gameName)
 * deleteGameInfoByGameID(gameID)
 *
 */


function openDB() {
    var db = LocalStorage.openDatabaseSync("savedGamesList_db", "", "Saved Games List", 1000);

    db.transaction(function(tx){
        tx.executeSql('CREATE TABLE IF NOT EXISTS listofsavedgames( revisionDate DATE, id INTEGER PRIMARY KEY, gameName TEXT, gameID TEXT, savedGameFile TEXT )');
        tx.executeSql('CREATE TABLE IF NOT EXISTS      listofgames( revisionDate DATE, id INTEGER PRIMARY KEY, gameName TEXT, gameID TEXT, format TEXT, url TEXT )');
    });

    try {
        if (db.version === "") {
            console.log("Upgrading database from %1 -> v0.2".arg(db.version))

            db.changeVersion(db.version, "0.2", function() {});
        }
        /* Schema Upgrade to v0.2 */
        if (db.version === "0.1") {
            console.log("Upgrading database from %1 -> v0.2".arg(db.version))

            db.changeVersion(db.version, "0.2", function(tx) {
                tx.executeSql('ALTER TABLE listofsavedgames ADD gameID TEXT');
                tx.executeSql('UPDATE listofsavedgames SET gameID=""');
            });
        }

    } catch (err) {
        console.log("Error creating table in database: " + err)
    } return db
}

// Functions for saved games
function storeSavedGame(revisionDate, gameName, gameID, savedGameFile) {
    var db = openDB();

    db.transaction(function(tx){
        tx.executeSql(
            'INSERT OR REPLACE INTO listofsavedgames(revisionDate, gameName, gameID, savedGameFile) VALUES(?, ?, ?, ?)',
            [revisionDate, gameName, gameID, savedGameFile]
        );
    });
}

function getGameFromSaved(savedGameName) {
    var gameName = "";
    var gameID = "";

    var db = openDB();

    db.transaction(function(tx) {
        var rs = tx.executeSql(
            'SELECT gameName, gameID FROM listofsavedgames WHERE savedGameFile=? ORDER BY revisionDate DESC;',
            [savedGameName]
        );

        if (rs.rows.length > 0) {
            gameName = rs.rows.item(0).gameName;
            gameID = rs.rows.item(0).gameID;
        } else {
            gameName = "Unknown";
        }
    });

    return {"gameName":gameName, "gameIFID": gameID};
}

function deleteSavedGameFromList(savedGameName){
    var db = openDB();

    try {
        db.transaction(function(tx){
            tx.executeSql(
                'DELETE FROM listofsavedgames WHERE savedGameFile=?;',
                [savedGameName]
            );
        });
    } catch (err) {
        console.log("Error deleting row from database: " + err)
    }
}


//Functions for game files
function storeGameInfo(revisionDate, gameName, gameID, format, url) {
    var db = openDB();

    try {
        db.transaction(function(tx){
            tx.executeSql(
                'INSERT OR REPLACE INTO listofgames(revisionDate, gameName, gameID, format, url) VALUES(?, ?, ?, ?, ?)',
                [revisionDate, gameName, gameID, format, url]
            );
        });
    } catch (err) {
        console.log("storeGameInfo: Error storing game in database: " + err)
    }
}

function getGameInfo(gameName, gameID) {
    var gameIndex = "";
    var db = openDB();

    db.transaction(function(tx) {

        if (!gameID || gameID == "") {
            var rs = tx.executeSql(
                'SELECT id FROM listofgames WHERE gameName=? ORDER BY revisionDate DESC;',
                [gameName]
            );
        } else {
            var rs = tx.executeSql(
                'SELECT id FROM listofgames WHERE gameID=? ORDER BY revisionDate DESC;',
                [gameID]
            );
        }

        if (rs.rows.length > 0) {
            gameIndex = rs.rows.item(0).id;
        } else {
            gameIndex = null;
            console.log("getGameInfo: the requested game not found in the db");
        }
    });

    return gameIndex;
}

function getGameByUrl(gameUrl) {
    var game = null;

    var db = openDB();

    db.transaction(function(tx) {

        var rs = tx.executeSql(
            'SELECT * FROM listofgames WHERE url=? ORDER BY revisionDate DESC;',
            [gameUrl]
        );

        if (rs.rows.length > 0) {
            console.log("getGameByUrl: Found",rs.rows.length,"games")
            game = rs.rows.item(0);
        } else {
            console.log("getGameByUrl: Failed to get gameName and gameID");
        }
    });

    return game;
}

function updateFormat(id, format) {
    var db = openDB();

    try {
        db.transaction(function(tx){
            tx.executeSql('UPDATE listofgames SET format=? WHERE id=?;', [format, id]);
        });
    } catch (err) {
        console.log("updateFormat: Error updating format: " + err)
    }
}

function deleteGameInfoByName(gameName){
    var db = openDB();

    try {
        db.transaction(function(tx){
            tx.executeSql(
                'DELETE FROM listofgames WHERE gameName=?;',
                [gameName]
            );
        });
    } catch (err) {
        console.log("Error deleting row from database: " + err)
    }
}

function deleteGameInfoByGameID(gameID){
    var db = openDB();

    try {
        db.transaction(function(tx){
            tx.executeSql(
                'DELETE FROM listofgames WHERE gameID=?;',
                [gameID]
            );
        });
    } catch (err) {
        console.log("Error deleting row from database: " + err)
    }
}

function startGameByFormat(filePath, gameFormat, gameMd5sum) {
    //Known exceptions are fixed
    //if (!isException(gameMd5sum, filePath)) {
        switch (gameFormat.replace(" (non-authoritative)", "")) {
            case "advsys":
                mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "advsysremglk", "game": filePath, "pageTitle": i18n.tr("AdvSys interpreter")})
                break
            case "adrift":
                mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "scareremglk", "game": filePath, "pageTitle": i18n.tr("Adrift interpreter")})
                break
            case "agt":
                mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "agilityremglk", "game": filePath, "pageTitle": i18n.tr("Agility interpreter")})
                break
            case "alan":
                switch (filePath.slice(-4)) {
                    case ".a3c":
                        mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "alan3remglk", "game": filePath, "pageTitle": i18n.tr("Alan3 interpreter")})
                        break
                    case ".acd":
                        mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "alan2remglk", "game": filePath, "pageTitle": i18n.tr("Alan2 interpreter")})
                        break
                }
                console.log("Received alan, no filtype known")
                break
            case "blorbed glulx":
            case "glulx":
                mainPageStack.push(Qt.resolvedUrl("../PlayGlulx.qml"),{"game": filePath})
                break
            case "blorbed zcode":
            case "zcode":
                mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "bocfelremglk", "game": filePath, "pageTitle": i18n.tr("Bocfel interpreter")})
                break
            case "hugo":
                mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "hugoremglk", "game": filePath, "pageTitle": i18n.tr("Hugo interpreter")})
                break
            case "level9":
                mainPageStack.push(Qt.resolvedUrl("../PlayLevel9.qml"),{"game": filePath})
                break
            case "magscrolls":
                mainPageStack.push(Qt.resolvedUrl("../PlayMagnetic.qml"),{"game": filePath})
                break
            case "tads3":
            case "tads2":
                mainPageStack.push(Qt.resolvedUrl("../PlayTads2.qml"),{"game": filePath})
                break
            case "executable":
                //Check is it's a .dat or a better solution
                switch (filePath.slice(-4)) {
                    case ".zip":
                        console.log("Zip file")
                        mainView.extract(filePath)
                        break
                    case ".agt":
                    case ".D$$":
                        mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "agilityremglk", "game": filePath, "pageTitle": i18n.tr("Agility interpreter")})
                        break
                    //Default to scott is not a good idea
                    default:
                        mainPageStack.push(Qt.resolvedUrl("../PlayNoImage.qml"),{"terp": "scottremglk", "game": filePath, "pageTitle": i18n.tr("Scott Adams interpreter")})
                        break
                }
                break
            default:
                console.log("We didnt find the format. Format: " + gameFormat)
        }
    //}
}

function readBabelResult(babelResult) {
    var babelResultArray = babelResult.trim().split("\n")
    for (var j=0; j<babelResultArray.length; j++) {
        if (babelResultArray[j].indexOf("Format:") !== -1) {
            gameFormat = babelResultArray[j].trim();
            gameFormat = gameFormat.slice(gameFormat.indexOf("Format: ") + 8);
        }

        if (babelResultArray[j].indexOf("IFID") !== -1) {
            gameIFID = babelResultArray[j].trim().split(" ")[1]
        }

        console.log("babel " + j + " " + babelResultArray[j])
    }
}

function isException(md5sum, filePath) {
    var gameIsException = false;
    switch (md5sum.toLowerCase()) {
        //Scapeghost GAMEDAT1.DAT (PC)
        case "139d1f07b33b3178ab9a93d5dc61cfac":
        case "a8375aa2ca595694c315cb95e92b0ced":
        //Scapeghost GAMEDAT2.DAT (PC)
        case "6d86951023236d8a417663fb8dd1d94c":
        //Scapeghost GAMEDAT3.DAT (PC)
        case "f109218800ce4d9cd03c6b3d90e30010":
            console.log("Scapeghost PC")
            mainPageStack.push(Qt.resolvedUrl("../PlayLevel9.qml"),{"game": filePath})
            gameIsException = true;
            break
        default:
            console.log("We didn't find md5sum. MD5sum: " + md5sum)
    }

    return gameIsException;
}

function exceptionalIFID(md5sum) {
    var ifid = md5sum;
    switch (md5sum.toLowerCase()) {
        //Scapeghost GAMEDAT1.DAT (PC)
        case "139d1f07b33b3178ab9a93d5dc61cfac":
        case "a8375aa2ca595694c315cb95e92b0ced":
            console.log("Scapeghost PC")
            ifid ="LEVEL9-017";
            break
        //Scapeghost GAMEDAT2.DAT (PC)
        case "6d86951023236d8a417663fb8dd1d94c":
            console.log("Scapeghost PC")
            ifid ="LEVEL9-017";
            break
        //Scapeghost GAMEDAT3.DAT (PC)
        case "f109218800ce4d9cd03c6b3d90e30010":
            console.log("Scapeghost PC")
            ifid ="LEVEL9-017";
            break
        default:
            console.log("We didn't fin md5sum. MD5sum: " + md5sum)
    }

    return ifid;
}

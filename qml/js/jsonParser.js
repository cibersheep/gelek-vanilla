function formatText(json) {
    gameFlickable.previousContentHeight = gameFlickable.contentHeight

    if (settings.debugging) console.log("formatText")

    if (json.windows) {
        if (settings.debugging) console.log("Found windows. Get w and h graphics")

        json.windows.forEach(function(x){
            if (!!(x.type) && x.type == "graphics") {
                //Size of the UbuntuShape
                viewboxw = x.width;
                viewboxh = x.height;
                //Size of the Canvas img
                canvasWidth = x.graphwidth;
                canvasHeight = x.graphheight;
            }
        })
    }

    if (json.content) {
        for (var key in json.content) {
            var k = json.content[key]

            if (k.hasOwnProperty("text")) {
                var textWindowHeight = txt.height - gameFlickable.contentY;

                //ToDo: Clear should clear only the window that is in
                if (k.clear) {
                    txt.text = ""
                }

                for (var i = 0; i < k.text.length; i++) {
                    var jsonContent = k.text[i].content

                    if (jsonContent) {
                        var isPreText = false;
                        for (var j = 0; j < jsonContent.length; j++) {

                            //«Error de copia» shows images like this (inline?)
                            if (jsonContent[j].special) {
                                if(jsonContent[j].special = "image")

                                if (settings.debugging) console.log("We need to show CONTENT image: " + jsonContent[j].image + " from " + gameIFID);

                                canvasWidth = jsonContent[j].width
                                canvasHeight = jsonContent[j].height
                                viewboxw = jsonContent[j].width
                                viewboxh = jsonContent[j].height

                                var ctx = img.getContext("2d");

                                //Canvas images (url) needs to be loaded and when loaded, draw
                                img.imgToLoad = mainView.gamesFilesURL + gameIFID + "/chu_P" + jsonContent[j].image;
                                img.loadImage(img.imgToLoad);
                                break
                            }

                            if (jsonContent[j].text) {
                                jsonContent[j].text = jsonContent[j].text.replace(/&/g,"&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;")
                                if (settings.debugging) console.log(jsonContent[j].text)
                            }

                            switch (jsonContent[j].style) {
                                case "header":
                                    txt.text += "<font size='+2' color='" + darkColor + "'><b>" + jsonContent[j].text + "</b></font><br/>"
                                    if (settings.debugging) console.log(txt.text)
                                    break
                                case "subheader":
                                    txt.text += "<font size='+1' color='" + lightColor +"'><b><i>" + jsonContent[j].text + "</i></b></font>"
                                    break
                                case "emphasized":
                                    txt.text += "<i>" + jsonContent[j].text + "</i>"
                                    break
                                case "input":
                                    txt.text += "<i>" + jsonContent[j].text + "</i>"
                                    break
                                case "user1":
                                    txt.text += "<b>" + jsonContent[j].text + "</b>"
                                    break
                                case "user2":
                                    txt.text += "<u>" + jsonContent[j].text + "</u>"
                                    break
                                case "preformatted":
                                    var preText = jsonContent[j].text.replace(/  /g," &nbsp;")
                                    if (k.text[i].append) {
                                        if (txt.text.slice(-5, txt.text.length) == "<br/>") {
                                            if (settings.debugging) console.log("BR found " + txt.text.slice(-5, txt.text.length))
                                            txt.text = txt.text.slice(0, -5)
                                        }
                                    }

                                    //We need to add monospace to fix a bug in mopbile Text?
                                    txt.text += "<code style='font-family: monospace'>" + preText + "</code>"
                                    break
                                //For Visitantes
                                case "note":
                                case "alert":
                                    txt.text += "<font color='" + darkColor + "'>" + jsonContent[j].text + "</font>"
                                    break
                                case "special":
                                    break
                                default:
                                    txt.text += jsonContent[j].text

                                    //We might need to convert this to Inform format
                                    //if (gameName === "") getNameYear(jsonContent[j].text);
                            }
                        }
                    } else {
                        if (settings.debugging) console.log("Empty line")
                        txt.text += "<br/>"
                        //skip the end of the loop to place only one br
                        continue
                    }

                    txt.text += "<br/>"
                }

                var textHeightToUpdate = gameFlickable.contentHeight - gameFlickable.previousContentHeight;
                flickText(textHeightToUpdate);
            }

            //Get grid window height
            if (json.windows) {
                for (var n = 0; n <= json.windows.length -1; n++) {
                    if (json.windows[n].type == "grid") {
                        var gridwindowID = json.windows[n].id
                        if (settings.debugging) console.log("We have an id: " + gridwindowID + " is = " + json.windows[n].id)
                        gridHeight = json.windows[n].gridheight
                        if (settings.debugging) console.log("Grid Height is " + json.windows[n].gridheight)
                    }
                }
            }

            //Lines
            if (k.hasOwnProperty("lines")) {
                //Take the id from the windows we found the lines in
                if (settings.debugging && json.content[k])
                    console.log("gridwindowID " + gridwindowID + ". lines.id " + json.content[k].id)

                for (var l = 0; l <= gridHeight -1; l++) {
                    if (k.lines[l]) {
                        var jsonLines = k.lines[l].content
                        var jsonLineLine = k.lines[l].line

                        if (jsonLines) {
                            var thisLine = []

                            for (var m = 0; m <= jsonLines.length -1; m++) {
                                if (jsonLines[m].line){
                                    console.log("Line " + jsonLines[m].line)
                                }

                                //jsonLines[m].text = jsonLines[m].text.replace(/</g, "\<").replace(/>/g, "\>")

                                switch (jsonLines[m].style) {
                                    //TODO: Implement user1 and user2 styles
                                    case "subheader":
                                        thisLine.push( { "text": jsonLines[m].text, "bold": true, "italic": false, "color": "#000", "bgcolor": "#fff"} )
                                        break
                                    case "preformatted":
                                        thisLine.push( { "text": jsonLines[m].text, "bold": false, "italic": false, "color": darkColor, "bgcolor": "#fff"} )
                                        break
                                    //For Magnetic Scrolls games
                                    case "user1":
                                        thisLine.push( { "text": jsonLines[m].text, "bold": true, "italic": false, "color": lightColor, "bgcolor": "#000"} )
                                        break
                                    case "user2":
                                        thisLine.push( { "text": jsonLines[m].text, "bold": true, "italic": false, "color": darkColor, "bgcolor": "#dfdfdf"} )
                                        break
                                    case "alert":
                                        thisLine.push( { "text": jsonLines[m].text, "bold": false, "italic": false, "color": "#fff", "bgcolor": "#000"} )
                                        break
                                    case "emphasized":
                                        thisLine.push( { "text": jsonLines[m].text, "bold": false, "italic": true, "color": "#000", "bgcolor": "#fff"} )
                                        break
                                    case "normal":
                                    default:
                                        thisLine.push( { "text": jsonLines[m].text, "bold": false, "italic": false, "color": "#000", "bgcolor": "#fff"} )
                                }
                            }

                            //Check if we are trying to add a new line or replace an existing one
                            if (gridModel.count > jsonLineLine) {
                                //Replace an existing line
                                //gridModel.set() doesn't seem to work. A workaround is to remove and insert thisLine
                                gridModel.remove(
                                    jsonLineLine
                                );

                                gridModel.insert(
                                    jsonLineLine,
                                    { "singleRow": thisLine }
                                );
                            } else {
                                //Add a new line
                                gridModel.append(
                                    { "singleRow": thisLine }
                                );
                            }
                        }
                    }
                }

                //Clean any exiding line i.e. the grid is shorter than before
                if (gridHeight < gridModel.count) {
                    if (settings.debugging) console.log("Our grid model is longer than the grid height")
                    for (var p = gridModel.count-1; p >= gridHeight; p--) {
                        gridModel.remove(p);
                    }
                }
            }

            if (k.hasOwnProperty("draw")) {
                if (settings.debugging) console.log("We have an image")
                var xDraw;
                var yDraw;
                var wDraw;
                var hDraw;

                for (var j=0; j<k.draw.length; j++){
                    xDraw = 0;
                    yDraw = 0;
                    wDraw = viewboxw;
                    hDraw = viewboxh;
                    var ctx = img.getContext("2d");
                    ctx.fillStyle = palette(k.draw[j].color)

                    switch (k.draw[j].special) {
                        case "setcolor":
                            console.log("TODO: Setcolor is not catched:",k.draw[j].color)
                            break
                        case "fill":
                            if (k.draw[j].hasOwnProperty("x")) {
                                xDraw = k.draw[j].x;
                                wDraw = k.draw[j].width;
                            }

                            if (k.draw[j].hasOwnProperty("y")) {
                                yDraw = k.draw[j].y;
                                hDraw = k.draw[j].height;
                            }

                            //The draw[1] must be the size of the image. We'll use it as cropSize
                            if (j == 2 && clipHeight == -1) {
                                if (settings.debugging) console.log("j 1 rectangle (should be image size):",wDraw,hDraw)
                                clipWidth = wDraw;
                                clipHeight = hDraw

                                if (settings.debugging) console.log("j 1",viewboxw,viewboxh)
                            }

                            ctx.fillRect(xDraw, yDraw, wDraw, hDraw);
                            img.requestPaint()
                            break
                        case "image":
                            if (settings.debugging) console.log("We need to show image: " + k.draw[j].image + " from " + mainView.gamesFilesURL + gameIFID);
                            //Make sure the canvas and the image have the same size (useful when images with different sizes mixed)
                            canvasWidth = k.draw[j].width
                            canvasHeight = k.draw[j].height
                            viewboxw = k.draw[j].width
                            viewboxh = k.draw[j].height
                            img.imgX = k.draw[j].x; //unused
                            img.imgY = k.draw[j].y; //unused

                            //Canvas images (url) needs to be loaded and when loaded, draw
                            img.imgToLoad = mainView.gamesFilesURL + gameIFID + "/chu_P" + k.draw[j].image;
                            img.loadImage(img.imgToLoad);
                        default:
                    }
                }
            }

        } //forEach
    }

    /* Mole and Archers sagas, if played with graphics
     * after selection an option, the game waits for
     * timer updates.
     * It supposed to set a repeating timer. In this way: input
     * from player should go into a pile to avoid conflicting
     * gen number...
     */
    if (json.timer) {
        if (settings.debugging) console.log("Timer interval set to: " + json.timer)
        retrieveTimer.interval = json.timer
        retrieveTimer.start()
    }

    if (json.timer === null) {
        if (settings.debugging) console.log("Timer is null: " + json.timer)
        retrieveTimer.stop()
    }

    //Take care of special input for saving or restore
    if (json.specialinput) {
        if (json.specialinput.type === "fileref_prompt") {
            GelekBackend.createSavedGamesDir()
            switch (json.specialinput.filemode) {
                case "read":
                    PopupUtils.open(showPopUpRestore)
                    break
                //TADS marks save as "readwrite"
                case "readwrite":
                case "write":
                    if (settings.debugging) console.log("Special Input:",json.specialinput.filemode);
                    PopupUtils.open(showPopUpSave)
                    break
                case "writeappend":
                    if (settings.debugging) console.log("Special Input: Case writeappend");
                    break
            }
        }
    }

    //If there's more commands in the buffer and we have no timer running
    //take the next command
    if (commandsBuffer.length > 0) {
        if (settings.debugging) console.log("Command to the buffer")
        sendCommandToGtk(commandsBuffer.shift());
    } else if (settings.debugging) console.log("Command NOT in the buffer",commandsBuffer.length,retrieveTimer.running)

    //flickable.flick(0,-playLevel9.height -levelMainText.height)
    //flickable.contentY += levelMainText.height - playLevel9.height
    if (settings.debugging) console.log("++--- Gen: " + response.input[0].gen + " : " + response.gen)
}

function palette(color) {
    switch (color) {
        case "#AFEEEE": //blue
            return "#bef2ff";
            break
        case "#8B5742": //Maroon
            return "#c44114";
            break
        case "#5CACEE": //dark blue
            return "#19b6ee";
            break
        case "#EE2C2C": //red
            return "#ed3146";
            break
        case "#EEC900": //yellow
            return "#f5d412";
            break
        case "#43DC80": //green
            return "#3eb34f";
            break
        default:
            return color
    }
}

function applyStylehint(style) {
    //Not finished. Do
    //Read: http://adamcadre.ac/gull/gull-2e.html

    /* Els estils van per finestres.
     *
     * {"reverse": 1}
     * margin-left:
     * text-indent
     * text-align: justify, center, right
     * font-size
     * font-weight - lighter o bold o normal
     * font-style - italic o normal
     * font-family - var(--glkote-prop-family) o var(--glkote-mono-family)
     * color
     * background-color
     */
    switch (val) {
        case style_Normal:
            return "normal";
        case style_Emphasized:
            return "emphasized";
        case style_Preformatted:
            return "preformatted";
        case style_Header:
            return "header";
        case style_Subheader:
            return "subheader";
        case style_Alert:
            return "alert";
        case style_Note:
            return "note";
        case style_BlockQuote:
            return "blockquote";
        case style_Input:
            return "input";
        case style_User1:
            return "user1";
        case style_User2:
            return "user2";
        default:
            return "unknown";
    }
}

//Checks if a json is valid = not splitted in different stdouts
function isJsonFinished(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        if (settings.debugging) console.log("isJsonFinished error:",e)
        return false;
    }
    return true;
}

function sendCommandToGtk(command) {
    //Make char input accepted from TextField
    //                                    and no timer?
    //TODO: Check if remglk asks for input or not
    //TODO: Get input line window at the begining, nnot every time
    for (var i = 0; i < response.input.length; i++) {
        if (response.input && response.input[i].type === "char") {
            sendCharToGtk(command, i);
        } else if (command !== "") {
            GelekBackend.sendCommand('{ "type":"line", "gen":' + response.gen + ', "window":' + response.input[i].id + ', "value": "' + command + '" }') //Should be command.text.slice(response.input[0].maxlen)
        }
    }
}

function sendCharToGtk(singleChar, windowid) {
        var character

        switch (singleChar) {
            case "": //If we accept the text, is a return
                character = "return"
                break
            case " ": //If we accept the text, is a space
                character = " "
                break
            case "up":
            case "down":
            case "right":
            case "left":
            case "tab":
                if (settings.debugging) console.log("singleChar is",singleChar)
                character = singleChar
                break
            default: //Make sure we send only the first letter
                if (settings.debugging) console.log("We gor a string as character:", singleChar)
                character = singleChar.slice(0,1)
        }

        GelekBackend.sendCommand('{ "type":"char", "gen":' + response.gen + ', "window":' + response.input[windowid].id + ', "value": "' + character + '" }')

        if (settings.debugging) console.log("+---- Gen: " + response.input[windowid].gen + " : " + response.gen)
}

function keyPressedCheck(pressedKey) {
        var character

        switch (pressedKey) {
            case Qt.Key_Up:
                character = "up"
                break
            case Qt.Key_Down:
                character = "down"
                break
            case Qt.Key_Right:
                character = "right"
                break
            case Qt.Key_Left:
                character = "left"
                break
            case Qt.Key_Tab:
                character = "tab"
                break
            default: //Make sure we send only the first letter
                return false;
        }

        if (settings.debugging) console.log("keyPressedCheck", pressedKey, character)

        if (response.input[0].type === "char" && !retrieveTimer.running) {
            GelekBackend.sendCommand('{ "type":"char", "gen":' + response.gen + ', "window":' + response.input[0].id + ', "value": "' + character + '" }')
        }

        return true;
}

//Add current command to a buffer (important to not get wrong gen)
function commandToBuffer(command) {
    //Add the latest command to the buffer
    commandsBuffer.push(command);

    //Make sure that if we don't have a timer, to send the last command to GLK
    if (!retrieveTimer.running) {
        sendCommandToGtk(commandsBuffer.shift());
    }
}

//Mainly used in the Timer retrieveTimer
function processBuffer(){
    //Dirty hack to never run into wrong generation number because Timer goes too quick
    //(before was in an else but picture stop drawing if entered a command while drawing)
    if (oldGeneration !== response.gen) {
        GelekBackend.sendCommand('{ "type":"timer", "gen":' + response.gen + ' }');
    }

    oldGeneration = response.gen
}

//Flick the text up to the top of the new text
function flickText(upToHere) {
    if (upToHere === untilTheEnd || upToHere <= gameFlickable.height) {
        while (!gameFlickable.atYEnd) {
            gameFlickable.contentY += units.dp(1);
        }

        showNextScroll = false;
        gameFlickable.scrollLeft = -1
    } else {
        gameFlickable.scrollLeft = upToHere - gameFlickable.height;
        upToHere = gameFlickable.height;
        showNextScroll = true
        upToHere = upToHere / units.dp(1);

        for (var i = 0; i <= upToHere; i++) {
            if (gameFlickable.atYEnd) {
                showNextScroll = false;
                break;
            }

            gameFlickable.contentY += units.dp(1);
        }
    }
}

function terminateTerp() {
    GelekBackend.stopTerp();
}
